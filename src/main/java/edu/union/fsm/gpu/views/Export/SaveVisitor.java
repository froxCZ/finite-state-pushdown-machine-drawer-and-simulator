

package edu.union.fsm.gpu.views.Export;

import edu.union.fsm.gpu.models.Edge;
import edu.union.fsm.gpu.models.Node;
import edu.union.fsm.gpu.models.NodeType;
import edu.union.fsm.gpu.views.PrintVisitor;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *visitor for saving
 * @author nicholasgoodrich
 */
public class SaveVisitor extends PrintVisitor {
    FileWriter writer;
    public SaveVisitor(FileWriter w){
        writer = w;
    }
    /**
     * visitor for nodes
     * @param n 
     */
    @Override
    public void visit(Node n){
        try {
            writer.append("Node,");
            writer.append(n.getLabel().getRawLabel());
            writer.append(",");
            writer.append(n.getPosition().toString());
            writer.append(",");
            writer.append(String.valueOf(n.hashCode()));
            writer.append(",");
            String i = n.getType().toString();
            String typeString= "";
                if(i.contains("FinishType")){typeString+="FinishType";}
                if(i.contains("NormalType")){typeString+="NormalType";}
                if(i.toString().contains("StartType")){typeString+="StartType";}
            writer.append(typeString);
            writer.append("\n");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    /**
     * visitor for edges
     * @param e 
     */
    public void visit(Edge e){
        try {
            writer.append("Edge,");
            writer.append(e.getLabel().getRawLabel());
            writer.append(",");
            writer.append(String.valueOf(e.getFromNode().hashCode()));
            writer.append(",");
            writer.append(String.valueOf(e.getToNode().hashCode()));
            writer.append(",");
            writer.append(e.getLineMiddlePoint().toString());
            writer.append("\n");
        } catch (IOException ex) {
           ex.printStackTrace();
        }
    }
    
}
