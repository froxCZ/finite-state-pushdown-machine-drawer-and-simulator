

package edu.union.fsm.gpu.views.display;

import edu.union.fsm.gpu.Machine;
import edu.union.fsm.gpu.models.Edge;
import edu.union.fsm.gpu.models.Node;
import edu.union.fsm.gpu.models.TemplateObject;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;

/**
 *  
 * @author frox
 */
public class DisplayView {
    public static Graphics2D g2d;
    public static Color COLOR_SELECTED = Color.BLUE;
    public static Color COLOR_DEFAULT = Color.BLACK;
    static Color FILL_COLOR_DEFAULT = Color.WHITE;
    /**
     * draws fsm on g
     * @param g
     * @param fsm 
     */
    public static void draw(Graphics g,Machine fsm){
        for(Edge e: fsm.getEdges()){
             e.accept(new DisplayVisitor((Graphics2D)g));
        }           
        for(Node n: fsm.getNodes()){
             n.accept(new DisplayVisitor((Graphics2D)g));
        }
        
    }
}
