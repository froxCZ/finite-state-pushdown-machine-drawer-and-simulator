

package edu.union.fsm.gpu.views.display;

import edu.union.fsm.gpu.models.Edge;
import edu.union.fsm.gpu.models.Label;
import edu.union.fsm.gpu.models.Node;
import edu.union.fsm.gpu.views.PrintVisitor;
import java.awt.Graphics2D;

/**
 *
 * @author frox
 */
public class DisplayVisitor extends PrintVisitor{
    Graphics2D g2d;

    public DisplayVisitor(Graphics2D _g2d) {
        g2d = _g2d;
    }
    
    @Override
    public void visit(Node n){
        NodeView.getInstance().print(g2d,n);
    }
    @Override
    public void visit(Edge e){
        EdgeView.getInstance().print(g2d,e);
    }
}
