

package edu.union.fsm.gpu.views.display;

import edu.union.fsm.gpu.Machine;
import edu.union.fsm.gpu.Simulator;
import edu.union.fsm.gpu.models.Edge;
import edu.union.fsm.gpu.models.Node;
import edu.union.fsm.gpu.models.TemplateObject;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

/**
 *  
 * @author frox
 */
public class SimulatorView {
    public static Graphics2D g2d;
    public static Color COLOR_SELECTED = Color.BLUE;
    public static Color COLOR_DEFAULT = Color.BLACK;
    static Color FILL_COLOR_DEFAULT = Color.WHITE;
    static Color ERROR = Color.RED;
    static Color AT_STATE = Color.GREEN;
    /**
     * draws machine on g
     * @param g
     * @param fsm 
     */
    public static void draw(Graphics g,Simulator simulator){
        for(Edge e: simulator.machine.getEdges()){
             e.accept(new SimulatorVisitor((Graphics2D)g,simulator));
        }           
        for(Node n: simulator.machine.getNodes()){
             n.accept(new SimulatorVisitor((Graphics2D)g,simulator));
        }             
    }
}
