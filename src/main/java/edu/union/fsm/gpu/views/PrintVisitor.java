

package edu.union.fsm.gpu.views;

import edu.union.fsm.gpu.models.Edge;
import edu.union.fsm.gpu.models.Label;
import edu.union.fsm.gpu.models.Node;

/**
 *
 * @author frox, ngoodrich, palacet
 */
public abstract class PrintVisitor extends Visitor {
    public abstract void visit(Node n);
    public abstract void visit(Edge e);
}
