

package edu.union.fsm.gpu.views.display;

import edu.union.fsm.gpu.models.Label;
import edu.union.fsm.gpu.helpers.Position;
import edu.union.fsm.gpu.models.Edge;
import edu.union.fsm.gpu.models.Node;
import edu.union.fsm.gpu.models.TemplateObject;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

/**
 *
 * @author frox
 */
public class LabelView{
    public static LabelView instance = null; 
    private static final int BOTTOM = 5;
    private static final int PIX = 7;
    public static int FONTSIZE =12;
    public static LabelView getInstance(){
        if(instance == null) {instance = new LabelView();}
        return instance;
    }    
    /**
     * prints label for edge
     * @param g2d
     * @param l
     * @param e 
     */
    public void print(Graphics2D g2d,Label l,Edge e) {
        Position p = e.getLineMiddlePoint();
        Font f = new Font("Ariel", Font.PLAIN, FONTSIZE);
        g2d.setFont(f);        
      //  g2d.drawString(l.getRawLabel(), p.getX(), p.getY());  
        int labelLength = l.getLabel().length();
        g2d.drawString(l.getLabel(),p.getX(),p.getY());
        Font expF = new Font("Ariel", Font.PLAIN, FONTSIZE-3);
        g2d.setFont(expF);
        g2d.drawString(l.getExponent(),p.getX()+(labelLength*PIX),p.getY()-BOTTOM);
        g2d.drawString(l.getSubscript(),p.getX()+(labelLength*PIX),p.getY()+BOTTOM);
    }
    /**
     * prints label for node
     * @param g2d
     * @param l
     * @param n 
     */
    public void print(Graphics2D g2d,Label l,Node n) {
        Position p = n.getPosition();
        Font f = new Font("Ariel", Font.PLAIN, FONTSIZE);
        g2d.setFont(f);
        Rectangle2D r = f.getStringBounds(l.getRawLabel(), g2d.getFontRenderContext());                
       // g2d.drawString(l.getRawLabel(), (int) (p.getX()-r.getCenterX()), p.getY()+NodeView.RADIUS+BOTTOM);
        g2d.drawString(l.getLabel(), (int) (p.getX()-r.getCenterX()), p.getY()+NodeView.RADIUS+BOTTOM);
        int labelLength = l.getLabel().length();
        Font expF = new Font("Ariel", Font.PLAIN, FONTSIZE-3);
        g2d.setFont(expF);
        g2d.drawString(l.getExponent(),(int) (p.getX()-r.getCenterX()+(labelLength*PIX)),p.getY()+NodeView.RADIUS);
        g2d.drawString(l.getSubscript(),(int) (p.getX()-r.getCenterX()+(labelLength*PIX)),p.getY()+NodeView.RADIUS+(BOTTOM*2));
    }    
}
