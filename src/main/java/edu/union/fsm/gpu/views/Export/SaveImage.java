

package edu.union.fsm.gpu.views.Export;

import edu.union.fsm.gpu.gui.Canvas;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

/**
 * Class for saving to an image
 * @author nicholasgoodrich
 */
public class SaveImage {
    //JPanel dPanel;
    private static String saveName;
    /**
     * saves an png image
     * @param dPanel
     * @param fileName
     * @return 
     */
    public static boolean save(Canvas dPanel,String fileName)
{
    BufferedImage bImg = new BufferedImage(dPanel.getWidth(), dPanel.getWidth(), BufferedImage.TYPE_INT_RGB);
    Graphics2D cg = bImg.createGraphics();
    dPanel.paintAll(cg);    
    try {
            if (ImageIO.write(bImg, "png", new File(fileName))) {return true;}
    } catch (IOException e) {
    }
    return false;
    
    
    }
}
