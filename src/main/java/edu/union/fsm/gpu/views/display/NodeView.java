
package edu.union.fsm.gpu.views.display;

import edu.union.fsm.gpu.FsmControl;
import edu.union.fsm.gpu.Simulator;
import edu.union.fsm.gpu.gui.Window;
import edu.union.fsm.gpu.models.Label;
import edu.union.fsm.gpu.helpers.Position;
import edu.union.fsm.gpu.models.FinishType;
import edu.union.fsm.gpu.models.Node;
import edu.union.fsm.gpu.models.StartType;
import edu.union.fsm.gpu.models.NodeType;
import edu.union.fsm.gpu.views.display.NodeTypeVisitor;
import static edu.union.fsm.gpu.views.display.DisplayView.g2d;
import java.awt.BasicStroke;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Line2D;
import java.util.Iterator;

/**
 *
 * @author frox, ngoodrich, palacet
 */
public class NodeView{
    public static int RADIUS = 40; 
    public static int STROKEWIDTH = 2;
    public static NodeView instance = null; 
    AffineTransform tx2 = new AffineTransform();
       
        
    public static NodeView getInstance(){
        if(instance == null) {instance = new NodeView();}
        return instance;
    }
    
    public void print(Simulator simulator, Graphics2D g2d,Node node){
        if(simulator.atStates.contains(node)){
            g2d.setColor(SimulatorView.AT_STATE);
        }else{
            g2d.setColor(DisplayView.COLOR_DEFAULT);
        }
        
        printShape(g2d,node);
    }
    
    /**
     * prints node graphic
     * @param g2d
     * @param node 
     */
    public void print(Graphics2D g2d,Node node) {
        if(node==Window.getSelectedObject()){
            g2d.setColor(DisplayView.COLOR_SELECTED);
        }else{
            g2d.setColor(DisplayView.COLOR_DEFAULT);
        }        
        printShape(g2d,node);
    }
    public void printShape(Graphics2D g2d,Node node){
        Label l = node.getLabel();
        if(l!=null){
            LabelView.getInstance().print(g2d,l, node);
        } 
        Position p = node.getPosition();
        this.drawNormalNode(g2d, p);
       //if(node.hasFinishType()){
       //    this.drawFinishNode(g2d, p);
           
       //}
       //if(node.hasStartType()){
       //    this.drawStartNode(g2d, p);           
       //}    
        Iterable<NodeType> types = node.getType();
        for(NodeType t : types){
            t.accept(new NodeTypeVisitor(g2d,p));
        }
       
    }
    
    private void setRadius(int r){
        RADIUS = r;
    }
    private void setStroke(int s){
        STROKEWIDTH = s;
    }
    public void scaleNode(int newRad,int newStroke){
        setRadius(newRad);
        setStroke(newStroke);
    }
    
    /**
     * 
     * @param p
     * @param n
     * @return if node is on position p
     */
    public boolean isOnPosition(Position p, Node n) {
        double xRadius = NodeView.RADIUS;
        double yRadius = NodeView.RADIUS;
        double distance = Math.hypot((double)(p.getX()- n.getPosition().getX()), (double)(p.getY()-n.getPosition().getY()));   
        return (distance <= xRadius && distance <= yRadius);          
    }
    /**
     * draw normalNode 
     * @param g2d
     * @param p 
     */
    private void drawNormalNode(Graphics2D g2d, Position p){
        g2d.setStroke(new BasicStroke(STROKEWIDTH));
        g2d.drawOval(p.getX()-RADIUS/2, p.getY()-RADIUS/2, RADIUS, RADIUS);
        g2d.setColor(DisplayView.FILL_COLOR_DEFAULT);
        g2d.fillOval(p.getX()-(RADIUS/2), p.getY()-RADIUS/2, RADIUS, RADIUS);
        
    }
    /**
     * draw startNode
     * @param g2d
     * @param p 
     */
    public void drawStartNode(Graphics2D g2d, Position p){
         g2d.setColor(DisplayView.COLOR_DEFAULT);
         int xTip =  p.getX();
         int yTip =  p.getY() - RADIUS/2;
         int xLeft =  xTip + 10;
         int yLeft =  yTip - 10;
         int xRight = xTip - 10;
         int yRight = yTip - 10;
         g2d.drawLine(xTip,yTip,xLeft,yLeft);
         g2d.drawLine(xTip,yTip,xRight,yRight);
         g2d.drawLine(xTip,yTip,xTip,yTip-50);
    }
    /**
     * draw finishNode
     * @param g2d
     * @param p 
     */
    public void drawFinishNode(Graphics2D g2d, Position p){
        g2d.setColor(DisplayView.COLOR_DEFAULT);
        g2d.drawOval(p.getX()-RADIUS/4, p.getY()-RADIUS/4, RADIUS/2, RADIUS/2);
    }
}
