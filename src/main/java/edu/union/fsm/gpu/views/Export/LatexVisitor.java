/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.union.fsm.gpu.views.Export;



import edu.union.fsm.gpu.models.Edge;
import edu.union.fsm.gpu.models.Node;
import edu.union.fsm.gpu.models.NodeType;
import edu.union.fsm.gpu.views.PrintVisitor;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author nicholasgoodrich
 */
public class LatexVisitor extends PrintVisitor{
    FileWriter writer;
    public LatexVisitor(FileWriter w) throws IOException{
        writer = w;
    }
     /**
     * visitor for nodes
     * @param n 
     */
    @Override
    public void visit(Node n){
        try {
            String i = n.getType().toString();
            String typeString= "";
                if(i.contains("FinishType")){typeString+="FinishType";}
                if(i.contains("NormalType")){typeString+="NormalType";}
                if(i.toString().contains("StartType")){typeString+="StartType";}
            writer.append("\n"+"\\draw [black]"+"("+n.getPosition().getX()+","+n.getPosition().getY()+")"
            +"circle ");
            if(i.contains("NormalType")&&!i.contains("FinishType")){
                writer.append("(3);"+"\n");
            }
            if(i.contains("NormalType")&& i.contains("FinishType")){
                writer.append("\n"+"\\draw [black]"+"("+n.getPosition().getX()+","+n.getPosition().getY()+")"
                +"circle ");
                writer.append("(2.4);");
            }
            writer.append("\\draw ("+ n.getPosition().getX() + "," + n.getPosition().getY() + ") node {$"+ n.getLabel().getLabel() +  "$};");
            
           
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
       @Override
       public void visit(Edge e){
        try {
            Node from = e.getFromNode();
            int xPosF= from.getPosition().getX();
            int yPosF= from.getPosition().getY();
            Node to = e.getToNode();
            int xPosT= to.getPosition().getX();
            int yPosT= to.getPosition().getY();
            if(from.equals(to)){
            writer.append("\n"+"\\draw [black] (");
            writer.append(xPosF+","+yPosF+") -- ("+xPosT+","+yPosT+") "+" arc (240.34019:-47.65981:2.25);");
            int midPointX = e.getLineMiddlePoint().getX();
            int midPointY = e.getLineMiddlePoint().getY();
            writer.append("\n"+"\\draw (" + midPointX + ","+ midPointY +") node [above] {$" +  e.getLabel().getLabel() +"$};");
            }
            else{
            writer.append("\n"+"\\draw [black] (");
            writer.append(xPosF+","+yPosF+") -- ("+xPosT+","+yPosT+");");
            int midPointX = e.getLineMiddlePoint().getX();
            int midPointY = e.getLineMiddlePoint().getY();
            writer.append("\n"+"\\draw (" + midPointX + ","+ midPointY +") node [below] {$" +  e.getLabel().getLabel() +"$};");
            }
        } catch (IOException ex) {
           ex.printStackTrace();
        }
    
        }
       
}
