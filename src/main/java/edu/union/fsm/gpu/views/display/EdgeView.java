
package edu.union.fsm.gpu.views.display;

import edu.union.fsm.gpu.FsmControl;
import edu.union.fsm.gpu.Simulator;
import edu.union.fsm.gpu.gui.Window;
import edu.union.fsm.gpu.models.Label;
import edu.union.fsm.gpu.helpers.Position;
import edu.union.fsm.gpu.models.Edge;
import edu.union.fsm.gpu.models.Node;
import java.awt.BasicStroke;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.geom.AffineTransform;
import java.awt.geom.Line2D;
import java.awt.geom.Path2D;

/**
 *
 * @author frox
 */
public class EdgeView{
    public static EdgeView instance = null; 
    public static int CLICK_INACCURACY = 10;
    public static int DEFAULT_STROKE = 1;
    public EdgeView(){
    }
    public static EdgeView getInstance(){
        if(instance == null) {instance = new EdgeView();}
        return instance;
    }
    public void print(Simulator simulator, Graphics2D g2d,Edge edge){
        g2d.setColor(DisplayView.COLOR_DEFAULT);
        Label l = edge.getLabel();
        if(l == null || l.getLabel().length() == 0){
            g2d.setColor(SimulatorView.ERROR);
        }
        printShape(g2d,edge);
    }
    
    
    /**
     * prints edge
     * @param g2d
     * @param edge 
     */
    public void print(Graphics2D g2d,Edge edge) {
        g2d.setStroke(new BasicStroke(DEFAULT_STROKE));
        if(edge==Window.getSelectedObject()){
            g2d.setColor(DisplayView.COLOR_SELECTED);
        }else{
            g2d.setColor(DisplayView.COLOR_DEFAULT);
        }      
        printShape(g2d,edge);
    }
    public void setStroke(int s){
        DEFAULT_STROKE = s;
    }
    
    
    private void printShape(Graphics2D g2d,Edge edge){
        g2d.draw(createLine(edge));
        this.drawArrowHead(edge, g2d);
        Label l = edge.getLabel();
        if(l!=null){
            LabelView.getInstance().print(g2d,l, edge);
        }         
    }
    boolean isOnPosition(Position p, Edge e) {
        return createLine(e).intersects(p.getX()-CLICK_INACCURACY/2, p.getY()-CLICK_INACCURACY/2, CLICK_INACCURACY, CLICK_INACCURACY);
    }
    /**
     * creates edge graphic, helper
     * @param edge
     * @return 
     */
    private Path2D.Double createLine(Edge edge){
        Node fromNode = edge.getFromNode();
        Node toNode = edge.getToNode();            
        Path2D.Double line = new Path2D.Double();
        line.reset();
        line.moveTo(fromNode.getPosition().getX(), fromNode.getPosition().getY());
        Position p = edge.getLineMiddlePoint(); //path point
        double xc = p.getX();
        double yc = p.getY();
        if(!fromNode.equals(toNode)){
            double x0 = fromNode.getPosition().getX();
            double y0 = fromNode.getPosition().getY();
            double x2 = toNode.getPosition().getX();
            double y2 = toNode.getPosition().getY();  
            //counting bezier control point
            double x1 = 2*xc - x0/2 - x2/2;
            double y1 = 2*yc - y0/2 - y2/2;
                
            line.curveTo(x0, y0, x1, y1, x2, y2);          
            return line;
        }
        else{
            double x0 = fromNode.getPosition().getX()+ NodeView.RADIUS/2;
            double y0 = fromNode.getPosition().getY();
            double x2 = toNode.getPosition().getX()-  NodeView.RADIUS/2; 
            double y2 = toNode.getPosition().getY(); 
            //counting bezier control point
            double x1 = 2*xc - x0/2 - x2/2;
            double y1 = 2*yc - y0/2 - y2/2;
                
            line.curveTo(x0, y0, x1, y1, x2, y2);          
            return line;
        }
        //counting bezier control point
//        double x1 = 2*xc - x0/2 - x2/2;
//        double y1 = 2*yc - y0/2 - y2/2;
//                
//        line.curveTo(x0, y0, x1, y1, x2, y2);          
//        return line;
    }
          /**
     * 
     * @param p1 starting point
     * @param p2 ending point
     * @return the distance between these two points
     */
       private double distanceCalc(Position p1, Position p2){   
            return  Math.sqrt((double)(p1.getX()-p2.getX())*(p1.getX()-p2.getX())
                           + (double) (p1.getY()-p2.getY())*(p1.getY()-p2.getY()));
     }
      /**
      * 
      * @param p1 point from
      * @param p2 point to travel
      * @param d distance 
      * @return 
      */
     private Position travelUpLine(Position p1,Position p2, double d){
         double x1 = p1.getX();
         double x2 = p2.getX();
         double y1 = p1.getY();
         double y2 = p2.getY();
         double xReturn = x2 - ((((double)NodeView.RADIUS)/(2.0*d))*(x2-x1));
         double yReturn = y2 - ((((double)NodeView.RADIUS)/(2.0*d))*(y2-y1));
         Position toReturn = new Position((int)Math.round(xReturn),(int)Math.round(yReturn));
         return toReturn;
     }
     private  void drawArrowHead(Edge e, Graphics2D g2d){
         int aLength = 15;
         Position from = e.getFromNode().getPosition();
         Position to = e.getToNode().getPosition();
         Position mid = e.getLineMiddlePoint();
         
        double xc = mid.getX();
        double yc = mid.getY();
        double x0 = from.getX();
        double y0 = from.getY();
        double x2 = to.getX();
        double y2 = to.getY();  
        
        //counting bezier control point
        Position controlPos = new Position((int)(2*xc - x0/2 - x2/2),(int)(2*yc - y0/2 - y2/2)); 
        double distance = this.distanceCalc(controlPos,to);
         Position tip = this.travelUpLine(controlPos,to,distance);
         double dx = controlPos.getX()-to.getX();
         double dy = controlPos.getY()-to.getY();
         double theta = Math.atan2(dy,dx);
         double phi = Math.toRadians(45);
         double x1 = tip.getX()+aLength*Math.cos(theta + phi);
         double y1 = tip.getY()+aLength*Math.sin(theta+phi);
         double x2a = tip.getX()+aLength*Math.cos(theta - phi);
         double y2a = tip.getY()+aLength*Math.sin(theta-phi);
         g2d.draw(new Line2D.Double(tip.getX(), tip.getY(), x1, y1));
         g2d.draw(new Line2D.Double(tip.getX(),tip.getY(),x2a,y2a));
     }
}
