/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.union.fsm.gpu.views.display;

import edu.union.fsm.gpu.helpers.Position;
import edu.union.fsm.gpu.models.FinishType;
import edu.union.fsm.gpu.models.NodeType;
import edu.union.fsm.gpu.models.Printable;
import edu.union.fsm.gpu.models.StartType;
import edu.union.fsm.gpu.views.Visitor;
import java.awt.Graphics2D;

/**
 *
 * @author nicholasgoodrich
 */
public class NodeTypeVisitor extends Visitor{
     Graphics2D g2d;
     Position P;

    public NodeTypeVisitor(Graphics2D _g2d,Position p) {
        g2d = _g2d;
        P=p;
    }
    public void accept(StartType n){
        NodeView.getInstance().drawStartNode(g2d, P);
    }
    
    public void accept(FinishType t){
        NodeView.getInstance().drawFinishNode(g2d, P);
    }
}
