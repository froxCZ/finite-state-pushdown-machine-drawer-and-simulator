
package edu.union.fsm.gpu.views.display;

import edu.union.fsm.gpu.helpers.Position;
import edu.union.fsm.gpu.models.Edge;
import edu.union.fsm.gpu.models.Node;
import edu.union.fsm.gpu.views.Visitor;

/**
 *
 * @author frox
 */
public class SelectVisitor extends Visitor{
    Position p;
    public SelectVisitor(Position pp) {
        p = pp;
    }
    /**
     * 
     * @param n
     * @return if node is on position p
     */
    public boolean visit(Node n){
        return NodeView.getInstance().isOnPosition(p,n);
    }
    /**
     * 
     * @param e
     * @return if edge is on position p
     */
    public boolean visit(Edge e){        
        return EdgeView.getInstance().isOnPosition(p,e);        
    }    
}
