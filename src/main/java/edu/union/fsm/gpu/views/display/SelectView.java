

package edu.union.fsm.gpu.views.display;

import edu.union.fsm.gpu.Machine;
import edu.union.fsm.gpu.helpers.Position;
import edu.union.fsm.gpu.models.Edge;
import edu.union.fsm.gpu.models.Node;
import edu.union.fsm.gpu.models.TemplateObject;

/**
 *selects which template object is on position
 * @author frox
 */
public class SelectView {        
    public static TemplateObject findObjectOnPosition(Machine fsm,Position p){ 
        for(Node n: fsm.getNodes()){
            if(n.accept(new SelectVisitor(p))==true) {return n;}
        }        
        for(Edge e: fsm.getEdges()){
            if(e.accept(new SelectVisitor(p))==true) {return e;}
        }
        return null;
    }
}
