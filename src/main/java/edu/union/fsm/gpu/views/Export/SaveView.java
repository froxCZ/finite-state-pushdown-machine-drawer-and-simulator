
package edu.union.fsm.gpu.views.Export;

import edu.union.fsm.gpu.Machine;
import edu.union.fsm.gpu.models.TemplateObject;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.List;

/**
 * Method for saving to csv
 * @author nicholasgoodrich
 */
public class SaveView {

    protected Machine fsm;
    public static void saveLatex(Machine fsm, FileWriter w) throws IOException {
        //TODO: saving to latex format
        w.append("\\documentclass[12pt]{article}\n" +
                       "\\usepackage{tikz}\n" +
                        "\n" +
                        "\\begin{document}\n" +
                        "\n" +
                        "\\begin{center}\n" +
                         "\\begin{tikzpicture}[scale=0.2]"
                         +"\n"+"\\tikzstyle{every node}+=[inner sep=0pt]");
        for(TemplateObject o: fsm.getObjects()){
            o.accept(new LatexVisitor(w));
        }
        w.append("\n"+"\\end{tikzpicture}\n" +
        "\\end{center}\n" +
         "\n" +
         "\\end{document}");
        w.flush();
        w.close();
    }
    protected Machine f;
    /**
     * given an fsm and a filewriter it saves the fsm to a csv
     * @param f
     * @param w
     * @throws IOException 
     */
    public static void save(Machine f,FileWriter w) throws IOException{
        for(TemplateObject o: f.getObjects()){            
            o.accept(new SaveVisitor(w));
        }
        w.flush();
        w.close();
    }
}
