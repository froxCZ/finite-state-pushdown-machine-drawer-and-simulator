
package edu.union.fsm.gpu.views.Export;

import edu.union.fsm.gpu.App;
import edu.union.fsm.gpu.Machine;
import edu.union.fsm.gpu.gui.Window;
import edu.union.fsm.gpu.helpers.Position;
import edu.union.fsm.gpu.models.FinishType;
import edu.union.fsm.gpu.models.Label;
import edu.union.fsm.gpu.models.Node;
import edu.union.fsm.gpu.models.PDAEdgeLabel;
import edu.union.fsm.gpu.models.StartType;
import edu.union.fsm.gpu.models.TemplateObject;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.TreeSet;

/**
 * Import from CSV class
 * @author nicholasgoodrich, palacet, frox
 */
public class ImportCSV {

    private Machine fsm;
    private BufferedReader csvReader;
    /**
     * constructs the ImportCSV with a string filename and a fsm F
     * @param filename
     * @param f
     * @throws FileNotFoundException 
     */
    public ImportCSV(String filename, Machine f) throws FileNotFoundException {
        csvReader = new BufferedReader(new FileReader(filename));
        this.fsm = f;
        this.csvReader = new BufferedReader(new FileReader(filename));

    }
    /**
     * performs the import of csv
     * @throws IOException 
     */
    public void Import() throws IOException {
        String line = "";
        ArrayList<String[]> edgesToLoad = new ArrayList<String[]>();
        fsm.clear();
        while ((line = csvReader.readLine()) != null) {
            String[] parsed = line.split(",");
            if (parsed[0].equals("Node")) {
                Position p = new Position((int) Integer.parseInt(parsed[2]), (int) Integer.parseInt(parsed[3]));
                Node n = new Node(p);
                if(parsed.length == 6 && parsed[5]!=null){//==6 means if it has defined type
                    if(parsed[5].contains("StartType")){n.addType(new StartType());}
                    if(parsed[5].contains("FinishType")){n.addType(new FinishType());}
                }
                if(parsed[1].length()>0){
                    System.out.println(parsed[1]);
                    Label l = new Label(parsed[1]);                
                    n.setLabel(l);
                }
                n.setHash(parsed[4]);                
                fsm.addNode(n);
            }else if (parsed[0].equals("Edge")) {
                edgesToLoad.add(parsed);
            }else{
                throw new IOException();
            }
        }
        for (String[] parsed : edgesToLoad) {
            Node nFrom = null;
            Node nTo = null;
            String fromNodeHash = String.valueOf(parsed[2].hashCode());
            String toNodeHash = String.valueOf(parsed[3].hashCode());
            Iterable<TemplateObject> addedObjects = fsm.getObjects();
            for (TemplateObject o : addedObjects) {
                if (String.valueOf(o.hashCode()).equals(fromNodeHash)) {
                    nFrom = (Node) o;
                }
                if (String.valueOf(o.hashCode()).equals(toNodeHash)) {
                    nTo = (Node) o;                    
                }
                if(nFrom!=null&&nTo!=null){
                    break;
                }

            }
            if(nFrom!=null&&nTo!=null){
                Label l = new Label();
                if(parsed[1].length()>0){
                    System.out.println(parsed[1]);
                    if(Window.instance.machineType == App.PUSHDOWN_MACHINE){
                        l = new PDAEdgeLabel(parsed[1]);
                    }else{
                        l = new Label(parsed[1]);                
                    }                    
                }                
                fsm._addEdge(nFrom, nTo,l);
            }                        
        }

    }
}
