

package edu.union.fsm.gpu.views.display;

import edu.union.fsm.gpu.Simulator;
import edu.union.fsm.gpu.models.Edge;
import edu.union.fsm.gpu.models.Label;
import edu.union.fsm.gpu.models.Node;
import edu.union.fsm.gpu.views.PrintVisitor;
import java.awt.Graphics2D;

/**
 *
 * @author frox
 */
public class SimulatorVisitor extends PrintVisitor{
    Graphics2D g2d;
    Simulator simulator;

    public SimulatorVisitor(Graphics2D _g2d,Simulator _simulator) {
        g2d = _g2d;
        simulator = _simulator;
    }
    
    @Override
    public void visit(Node n){
        NodeView.getInstance().print(simulator,g2d,n);
    }
    @Override
    public void visit(Edge e){
        EdgeView.getInstance().print(simulator,g2d,e);
    }
}
