/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.union.fsm.gpu;

import edu.union.fsm.gpu.gui.*;
import edu.union.fsm.gpu.gui.SimulatorListener;
import edu.union.fsm.gpu.models.Edge;
import edu.union.fsm.gpu.models.Node;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

/**
 *
 * @author frox
 */
public abstract class Simulator {
    public HashSet<Node> atStates = new HashSet<Node>();
    public HashSet<Node> startStates = new HashSet<Node>();
    public HashSet<Node> finishStates = new HashSet<Node>();
    public ArrayList<SimulatorListener> listeners = new ArrayList<SimulatorListener>();
    public String input;
    public int inputPointer;
    public Machine machine;
    public boolean reseted;    
    public Simulator(SimulatorListener _listener,Machine _fsm){
        listeners.add(_listener);
        machine = _fsm;    
        input = "";
    }
    public void reset(Collection _startStates,Collection _finishStates,String _input){
        inputPointer = 0;
        input = new String(_input);
        startStates.clear();
        startStates.addAll(_startStates);
        finishStates.clear();
        finishStates.addAll(_finishStates);
        atStates.clear();
        atStates.addAll(_startStates);
        invokeListeners();
    }
  

    public abstract void doStep();
    
    protected void invokeListeners(){
        for(SimulatorListener l: listeners){
            l.simulatorUpdated();
        }
    }   
    public void checkMachineValidity() throws InvalidMachineSetupException{
        allEdgesHasLabel();
        if(startStates.isEmpty())throw new InvalidMachineSetupException("no start state!");
        if(input.length()==0)throw new InvalidMachineSetupException("no input string!");
        if(finishStates.isEmpty())throw new InvalidMachineSetupException("no finish state!");        
    }

    protected boolean allEdgesHasLabel() throws InvalidMachineSetupException {
        for(Edge e:machine.getEdges()){
            if(e.getLabel() == null)throw new InvalidMachineSetupException("some edges does not have label!");
        }
        return true;
    }

    public boolean finished(){
        return inputPointer == input.length();
    }
    public boolean accepted(){
        if ((inputPointer == input.length())) {
            for(Node n:atStates){
                if(n.hasFinishType())return true;
            }
        }
        return false;
    }
    public boolean isAlive(){
        return atStates.size() > 0;
    }
}
