/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.union.fsm.gpu;

import edu.union.fsm.gpu.gui.Window;
import static edu.union.fsm.gpu.gui.commands.Command.window;
import edu.union.fsm.gpu.helpers.Position;
import edu.union.fsm.gpu.models.Node;
import edu.union.fsm.gpu.models.StartType;
import javax.swing.JOptionPane;

/**
 *
 * @author frox
 */

public class App {
    public static final int CANVAS_WIDTH = 600;
    public static final int CANVAS_HEIGHT = 600;
    public static String EXTENSION = ".gpu";
    public static String LATEX_EXTENSION = ".tex";
    public static final int FINITE_MACHINE = 10;
    public static final int PUSHDOWN_MACHINE = 11;    
    Window window;
    Machine machine;
    Simulator simulator;
    FsmControl fsmControl;
    public static void main(String[] args) {
        App app = new App();
        app.run();
    }    
    
    /**
     * Creates window, creates Machine and creates MachineController
     * 
     */
    public void run(){           

        
        window = new Window();   
        simulator = window.simulator;
        machine = window.machine;
        fsmControl = new FsmControl(window,machine);  
        Node n1 = new Node(new Position(30,30));
        n1.addType(new StartType());
        Node n2 = new Node(new Position(100,100));
        Node n3 = new Node(new Position(200,100));
        machine.addNode(n1);
        machine.addNode(n2);
        machine.addNode(n3);
        machine._addEdge(n1, n3);
        //window.showPopup("Press H for Help");
    }
}
