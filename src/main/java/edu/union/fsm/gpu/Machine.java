
package edu.union.fsm.gpu;

import edu.union.fsm.gpu.models.Label;
import edu.union.fsm.gpu.helpers.Position;
import edu.union.fsm.gpu.models.Edge;
import edu.union.fsm.gpu.models.FinishType;
import edu.union.fsm.gpu.models.Node;
import edu.union.fsm.gpu.models.NodeType;
import edu.union.fsm.gpu.models.StartType;
import edu.union.fsm.gpu.models.TemplateObject;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.TreeSet;
import java.util.Vector;

/**
 * This class is the model for the finite state machine, it contains the states and transitions
 * @author frox, ngoodrich, palacet
 */
public abstract class Machine {
    HashSet<TemplateObject> objects;
    protected FsmListener controlListener;
    
    public Machine(){
        objects = new HashSet<TemplateObject>();
    }
    /**
    *Given any template object state or transition and a label, the object will recieve a label
    *@param o 
    * @param l 
    */
    public void setObjectLabel(TemplateObject o ,Label l){
        o.setLabel(l);
        invokeListener();
    }
    /** 
     * Adds a node n to the finite state machine
     * @param n 
     */
    public void addNode(Node n){
       if(objects.contains(n)){ return;}
       objects.add(n);
       n.setFsm(this);
       invokeListener();
    }
    
    /**
     * Temporary method for testing purposes
     * adds an edge into the finite state machine given a start state and finish state 
     * @param from
     * @param to
     */    
    public void _addEdge(Node from,Node to){
        _addEdge(from,to,new Label());
    }
    public void _addEdge(Node from,Node to,Label l){
        Edge newEdge = new Edge(from,to);
        newEdge.setFsm(this);
        newEdge.setLabel(l);
        objects.add(newEdge);
        from.addOutEdge(newEdge);
        to.addInEdge(newEdge);
        invokeListener();        
    }
    /**
     * Removes a Node from the finite state machine
     * @param node to remove
     */
   public void _removeNode(Node toRemove){
       List<Edge> edgesToRemove = toRemove.getOutEdges();
       for(Edge e : edgesToRemove){
           objects.remove(e);
       }
       for(Edge e : toRemove.getInEdges()){
           objects.remove(e);
       }       
       objects.remove(toRemove);
       invokeListener();
       
   } 
   /**
    * Updates the control listener
    */
    public void invokeListener(){
        if(controlListener!=null) {controlListener.update();}
    }
    /**
     * sets the controlListener
     * @param l 
     */
    public void setControlListener(FsmListener l){
        controlListener = l;
    }    
    /**
     * sets the middle point of a transition
     * @param e
     * @param x
     * @param y 
     */
    public void setEdgeMiddlePoint(Edge e, int x, int y) {
        e.setLineMiddlePoint(x, y);
        invokeListener();
    }
    /**
     * given a node, this method sets the state type to finish state
     * @param n 
     */
    public void setNodeTypeFinish(Node n){
        n.addType(new FinishType());
        invokeListener();
    }
    /**
     * given a node, this method sets the state type to start state
     * @param n 
     */
    public void setNodeTypeStart(Node n){
        n.addType(new StartType());
        invokeListener();
    }
    public void toggleNodeType(Node n,NodeType t){
        n.toggleNodeType(t);
        invokeListener();
    }
    /**
     * given a node and two integers for the position, this method sets the location/position
     * of the node.
     * @param o
     * @param x
     * @param y 
     */
    public void setNodePosition(Node o, int x, int y) {
        if(y<0||x<0)return;
        o.setPosition(new Position(x,y));
        invokeListener();
    }
    /**
     * removes any TemplateObject (state or transition) from the finite state machine model
     * @param o 
     */
    public void remove(TemplateObject o ){
        if(objects.contains(o) == false){
            for(TemplateObject x: objects){
                System.out.println("in hashset: "+x.toString());
            }
            System.out.println(o.toString()+" is not in set!!");
        }
        objects.remove(o);
        o.remove();
        invokeListener();
    }
    /**
     * This clears the finite state machine model resulting in an empty fsm
     */
    public void clear(){
        objects =  new HashSet<TemplateObject>();
    }
    /**
     * returns the objects(states and transitions) of the finite state machine
     * @return objects
     */
    public Iterable<TemplateObject> getObjects(){        
        return objects;
    }
    public Collection<Node> getStartNodes(){
        ArrayList<Node> nodes = new ArrayList<Node>();        
        for(TemplateObject o: objects){
            if(o instanceof Node){
                Node n = (Node)o;
                if(n.hasStartType())nodes.add(n);
            }
        }
        return nodes;
    }
    public Iterable<Node> getNodes(){
        ArrayList<Node> nodes = new ArrayList<Node>();        
        for(TemplateObject o: objects){
            if(o instanceof Node) nodes.add((Node)o);
        }
        return nodes;
    }
    public Iterable<Edge> getEdges(){
        ArrayList<Edge> edges = new ArrayList<Edge>();        
        for(TemplateObject o: objects){
            if(o instanceof Edge) edges.add((Edge)o);
        }
        return edges;
    }    

    public Collection getFinishNodes() {
        ArrayList<Node> nodes = new ArrayList<Node>();        
        for(TemplateObject o: objects){
            if(o instanceof Node){
                Node n = (Node)o;
                if(n.hasFinishType())nodes.add(n);
            }
        }
        return nodes;        
    }
    /**
     * compares two templateObjects to see which has higher priority 
     */
    class TemplateObjectPriorityComparator implements Comparator<TemplateObject> {
        @Override
        public int compare(TemplateObject o1, TemplateObject o2) {
            if(o1.getPriority()<o2.getPriority()) { return -1;}
            else if(o1.hashCode()==o2.hashCode()) {return 0; }
            return 1;
        }
    }
    public abstract String typeToString();

}
