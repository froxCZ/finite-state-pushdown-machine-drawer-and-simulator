
package edu.union.fsm.gpu;

/**
 * Interface for FSMListener 
 * @author frox, ngoodrich, palacet
 */
public interface FsmListener {
    public void update();
}
