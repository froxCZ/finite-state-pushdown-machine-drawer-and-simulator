
package edu.union.fsm.gpu.helpers;

/**
 * Class for a position (x,y)
 * @author frox, ngoodrich, palacet
 */
public class Position {
    private int x;
    private int y;
    
    /**
     * creates a new position object with values xx and yy
     * @param xx
     * @param yy 
     */
    public Position(int xx,int yy){
        x = xx;
        y = yy;
    }

    /**
     * @return the x value of the position
     */
    public int getX() {
        return x;
    }

    /**
     * @param x the x value to set
     */
    public void setX(int x) {
        this.x = x;
    }

    /**
     * @return the y value of the position
     */
    public int getY() {
        return y;
    }

    /**
     * @param y the y value to set
     */
    public void setY(int y) {
        this.y = y;
    }
    /**
     * 
     * @return string representation of the position in form "x,y"
     */
    @Override
    public String toString(){
        String toReturn = "";
        toReturn = toReturn +  String.valueOf(this.getX()) + "," +  String.valueOf(this.getY());
        return toReturn;
    }
}
