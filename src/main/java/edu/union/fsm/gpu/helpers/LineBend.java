

package edu.union.fsm.gpu.helpers;

/**
 * Helper class for determining line bend
 * @author frox, ngoodrich, palacet
 */
public class LineBend {
    private double distanceFromStraightEdge;

    /**
     * @return the distanceFromStraightEdge
     */
    public double getDistanceFromStraightEdge() {
        return distanceFromStraightEdge;
    }

    /**
     * @param distanceFromStraightEdge the distanceFromStraightEdge to set
     */
    public void setDistanceFromStraightEdge(double distanceFromStraightEdge) {
        this.distanceFromStraightEdge = distanceFromStraightEdge;
    }
}
