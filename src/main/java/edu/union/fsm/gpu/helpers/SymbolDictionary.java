
package edu.union.fsm.gpu.helpers;

import java.util.*;
import javax.swing.JOptionPane;


/**
 *Helper class for the symbol dictionary. 
 * @author frox, palacet, ngoodrich
 */
public class SymbolDictionary {
    static final HashMap<String,String> dictionary = new HashMap<String,String>();
    static final String symbols = "Alpha,\u0386,Beta,\u0392,Gamma,\u0393,Delta,"
            +"\u0394,Epsilon,\u0395,Zeta,\u0396,Eta,\u0397,Theta,\u0398,Iota,\u0399,"
            + "Kappa,\u039A,Lambda,\u039B,Mu,\u039C,Nu,\u039D,Xi,\u039E,Omicron,\u039F,"
            + "Pi,\u03A0,Rho,\u03A1,Sigma,\u03A3,Tau,\u03A4,Upsilon,\u03A5,Phi,\u03A6,"
            + "Chi,\u03A7,Psi,\u03A8,Omega,\u03A9,alpha,\u03B1,beta,\u03B2,gamma,\u03B3,"
            + "delta,\u03B4,epsilon,\u03B5,zeta,\u03B6,eta,\u03B7,theta,\u03B8,iota,\u03B9,"
            + "kappa,\u03BA,lambda,\u03BB,mu,\u03BC,nu,\u03BD,xi,\u03BE,omicron,\u03BF,"
            + "pi,\u03C0,rho,\u03C1,sigma,\u03C3,tau,\u03C4,upsilon,\u03C5,phi,\u03C6,"
            + "chi,\u03C7,psi,\u03C8,omega,\u03C9,copyright,\u00A9";
    /**
     * builds the symbol dictionary
     */
    public SymbolDictionary(){
        this.buildDict();
    }
    /**
     * helper for building the dictionary of symbols
     */
    private void buildDict(){
        String[] values;
        values = symbols.split(",");
        for(int i = 0; i < values.length;i=i+2){
            dictionary.put(values[i],values[i+1]);
        }
    }
    /**
     * looks up a string s in the dictionary
     * @param s
     * @return the dictionary value or an error if string is null
     */
    public String lookUp(String s){
        String toReturn = dictionary.get(s);
        if(toReturn == null){
            
            //JOptionPane j = new JOptionPane();
            JOptionPane.showMessageDialog(null, "This is not a valid symbol or Syntax", "Error",
                                    JOptionPane.ERROR_MESSAGE);
            return "";
        }
        else{
            return toReturn;
        }
    }
    
}
