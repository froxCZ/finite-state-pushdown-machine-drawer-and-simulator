/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.union.fsm.gpu;

import edu.union.fsm.gpu.gui.*;
import edu.union.fsm.gpu.gui.SimulatorListener;
import edu.union.fsm.gpu.models.Edge;
import edu.union.fsm.gpu.models.Node;
import edu.union.fsm.gpu.models.PDAEdgeLabel;
import java.util.Collection;
import java.util.HashSet;

/**
 *
 * @author frox
 */
public class PDSimulator extends Simulator{
    public String stack = "";
    public String stack_backup="";
    public PDSimulator(SimulatorListener _listener, Machine _fsm) {
        super(_listener, _fsm);
    }
    
    @Override
    public void reset(Collection _startStates,Collection _finishStates,String _input){
        stack = stack_backup;
        super.reset(_startStates, _finishStates, _input);                
    }
    @Override
    public void checkMachineValidity() throws InvalidMachineSetupException{    
        super.checkMachineValidity();
        checkIfIsDeterministic();
        if(stack.length()==0)throw new InvalidMachineSetupException("no stack defined!");
    }

    @Override
    public void doStep() {
        HashSet<Node> newAtStates = new HashSet<Node>();
        boolean moved = false;
        for(Node n:atStates){
            for(Edge e: n.getOutEdges()){
                PDAEdgeLabel label = (PDAEdgeLabel)e.getLabel();
                if (label.getMustBeOnStack().equals(stack.substring(0, 1)) == false) continue;
                if(e.getLabel().getLabel().charAt(0) != input.charAt(inputPointer))continue;
                newAtStates.add(e.getToNode());                
                stack = stack.substring(1);
                stack = label.getPushOnStack() + stack;
                moved = true;
                break;
            }
            if(moved)break;
        }
        inputPointer++;
        atStates = newAtStates; 
        invokeListeners();        
    }

    private void checkIfIsDeterministic() throws InvalidMachineSetupException {
        for(Node n:machine.getNodes()){
            for(Edge e:n.getOutEdges()){
                for(Edge ee:n.getOutEdges()){
                    PDAEdgeLabel l = (PDAEdgeLabel)e.getLabel();                    
                    PDAEdgeLabel ll = (PDAEdgeLabel)ee.getLabel();
                    if(l.getLabel().equals("ε")){
                        throw new InvalidMachineSetupException("PD machine must be deterministic! Epsilon transition found");
                    }
                    if(l!=ll && l.getMustBeOnInput().equals(ll.getMustBeOnInput())&&l.getMustBeOnStack().equals(ll.getMustBeOnStack())){
                        System.out.println(l.getLabel()+ " "+ll.getLabel());
                        throw new InvalidMachineSetupException("PD machine must be deterministic! One state has 2 or more same transitions");
                    }
                }                
            }
        }
    }
    
}
