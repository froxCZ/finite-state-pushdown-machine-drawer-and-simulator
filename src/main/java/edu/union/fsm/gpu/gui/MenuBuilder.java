/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.union.fsm.gpu.gui;

import edu.union.fsm.gpu.gui.commands.*;
import java.awt.GridLayout;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextPane;

/**
 *
 * @author frox
 */
public class MenuBuilder {    
    public static EditorMenuPanel createMenuPanel(EditorTab editorTab){
        EditorMenuPanel menu = new EditorMenuPanel(4);   
        menu.addButton(new Button("Change canvas size",new ChangeCanvasSizeCommand()),1);
        menu.addButton(new Button("Save Png",new SavePngCommand()),1);
        menu.addButton(new Button("Save Latex",new SaveLatexCommand()),1);
        menu.addButton(new Button("Save Machine",new SaveFileCommand()),1);
        menu.addButton(new Button("Load Machine",new LoadFileCommand()),1);
        menu.addButton(new Button("Scale",new ScaleCommand()),1);
        
        menu.addButton(new SelectToolButton(editorTab, "Node" ,new NodeCreatorTool(editorTab)),2);            
        menu.addButton(new SelectToolButton(editorTab, "Edge" ,new EdgeCreatorTool(editorTab)),2);            
        menu.addButton(new SelectToolButton(editorTab, "Mouse" ,new MouseTool(editorTab)),2);   
        
        return menu; 
    }
   
    public static MenuPanel createMenuPanel(SimulatorTab simulatorTab){
        SimulatorMenuPanel menu = new SimulatorMenuPanel(4);         
        menu.addPlayButton(new Button(">",new OneStepSimulatorCommand()),2);
        menu.addPlayButton(new Button(">|",new OneStepSimulatorCommand()),2);        
        menu.addButton(new Button("Reset",new ResetSimulatorCommand()),2); 
        menu.addButton(new Button("Set input",new ShowMachineInputDialog()),2);                 
        return menu;       
    }    
}
