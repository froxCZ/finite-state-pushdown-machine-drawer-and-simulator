/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.union.fsm.gpu.gui;

import edu.union.fsm.gpu.gui.commands.SelectToolCommand;
import java.awt.Color;

/**
 *
 * @author frox
 */
public class SelectToolButton extends Button{
    public Tool tool;
    public SelectToolButton(EditorTab tab,String text,EditorTool _tool) {
        super(text,new SelectToolCommand(tab, _tool));
        tool = _tool;        
    }

    void select() {
        setBackground(Color.white);
    }

    void deselect() {
        setBackground(Color.lightGray);
    }
    
}
