/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.union.fsm.gpu.gui.commands;

import edu.union.fsm.gpu.views.display.EdgeView;
import edu.union.fsm.gpu.views.display.LabelView;
import edu.union.fsm.gpu.views.display.NodeView;

/**
 *
 * @author nicholasgoodrich
 */
public class ScaleCommand extends Command{
    public int radius;
    public int stroke;
    @Override
    public void execute(){
       String scaleAmount =  window.showInputDialog("Scale amount can be 1(Normal) 2(Medium), 3(Large)", null);
       if(scaleAmount.contains("1")){
           NodeView.RADIUS=40;
           EdgeView.DEFAULT_STROKE=1;
           LabelView.FONTSIZE=12;
       }
       else if(scaleAmount.contains("2")||scaleAmount.contains("3")){
       NodeView.RADIUS = Integer.parseInt(scaleAmount)*NodeView.RADIUS;
       EdgeView.DEFAULT_STROKE=Integer.parseInt(scaleAmount);
       LabelView.FONTSIZE=LabelView.FONTSIZE*Integer.parseInt(scaleAmount);
       window.repaint();
       }
    }
    
}
