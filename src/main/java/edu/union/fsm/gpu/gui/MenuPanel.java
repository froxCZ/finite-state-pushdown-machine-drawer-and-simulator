/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.union.fsm.gpu.gui;

import java.awt.Color;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JPanel;

/**
 *
 * @author frox
 */
public class MenuPanel extends JPanel { 
    protected final Set<Button> allButtons;
    protected JPanel[] rows;
    public MenuPanel(int rows_cnt){
        super();
         rows = new JPanel[rows_cnt];
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        for(int i=0;i<rows.length;i++){
            rows[i] = new JPanel();
            add(rows[i]);
        }        
        allButtons = new HashSet<Button>();
        this.setBackground(Color.GRAY);   
    }
     
    public void addButton(Button b,int row){
        rows[row].add(b);
        allButtons.add(b);
    }
    public void addComponent(JComponent c,int row){
        rows[row].add(c);
    }
    public void disableButtons(){
        for(Button b:allButtons){;
            b.setEnabled(false);
        }
    }
    public void enableButtons(){
        for(Button b:allButtons){;
            b.setEnabled(true);
        }
    }    
}
