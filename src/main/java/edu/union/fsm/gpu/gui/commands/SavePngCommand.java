/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.union.fsm.gpu.gui.commands;

import edu.union.fsm.gpu.gui.Canvas;
import edu.union.fsm.gpu.views.Export.SaveImage;
import javax.swing.JFrame;

/**
 *
 * @author frox
 */
public class SavePngCommand extends Command {

    @Override
    public void execute() {
        Canvas j = window.editorPanel.canvas;
        j.createImage(j.getHeight(), j.getWidth());
        String filePath = window.showFileSaverAndGetPath(".png");
        if (filePath == null || SaveImage.save(j, filePath)) {
            window.showPopup("File saved.");
        } else {
            window.showPopup("Error. File was not saved");
        }
    }

}
