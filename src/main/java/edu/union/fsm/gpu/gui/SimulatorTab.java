/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.union.fsm.gpu.gui;

import java.awt.Color;
import javax.swing.JLabel;

/**
 *
 * @author frox
 */
public class SimulatorTab extends Tab{
    JLabel machineInputJLabel;
    JLabel statusJLabel;
    public final int STATUS_MSG = 0;   
    public final int ACCEPT_MSG = 1;
    public final int REJECTED_MSG = 2;
    public final int ERROR_MSG = 3; 
    public SimulatorTab(Window w){
        super(w);
        menuPanel = MenuBuilder.createMenuPanel(this);
        machineInputJLabel = new JLabel();        
        statusJLabel = new JLabel();
        canvas = new SimulatorCanvas(w);
        addMenu(menuPanel);
        addCanvas(canvas);
        menuPanel.addComponent(new JLabel("Status:\t"),0);
        menuPanel.addComponent(statusJLabel, 0);        
        menuPanel.addComponent(new JLabel("Machine input:\t"),1);
        menuPanel.addComponent(machineInputJLabel,1);
        this.setBackground(Color.white);
    }
    @Override
    public void update() {
        canvas.repaint();
        String guiMachineInput = "";
        String machineInput = window.simulator.input;
        int inputPointer = window.simulator.inputPointer;
        guiMachineInput = machineInput.substring(0, inputPointer)+"<font color='blue'>"+machineInput.substring(inputPointer)+"</font>";             
        machineInputJLabel.setText("<html>"+guiMachineInput+"</html>");
        if(window.simulator.finished()){
            if(window.simulator.accepted()){
                setAccepted();
            }else{
                setRejected();
            }
            menuPanel.disableButtons();
        }else{
            if(window.simulator.isAlive()){
                setStatus("running...",STATUS_MSG);   
            }else{
                setRejected();
            }         
        }
    }
    public void setStatus(String msg,int level){        
        String color;
        switch(level){
            case REJECTED_MSG:
            case ERROR_MSG:
                color = "red";
                break;
            case ACCEPT_MSG:
                color = "green";
                break;        
            default:
                color="black";
        }
        statusJLabel.setText("<html><font color="+color+">"+msg+"</html>");
    }
    public void setErrorMsg(String msg){
        setStatus(msg, ERROR_MSG);
    }
    public void setReady(){
        setStatus("ready", STATUS_MSG);
    }

    private void setAccepted() {
        setStatus("accepted",ACCEPT_MSG);
        menuPanel.disableButtons();
    }

    private void setRejected() {
        setStatus("not accepted",REJECTED_MSG);
        menuPanel.disableButtons();
    }
}
