/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.union.fsm.gpu.gui;

/**
 *
 * @author frox
 */
public class InvalidMachineSetupException extends Exception{

    public InvalidMachineSetupException(String some_edges_does_not_have_label) {
        super(some_edges_does_not_have_label);
    }
    
}
