/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.union.fsm.gpu.gui;

import edu.union.fsm.gpu.gui.commands.Command;
import java.awt.Color;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.JButton;

/**
 *
 * @author frox
 */
public class Button extends JButton{    
    Command command;    
    public Button(String text,Command _command){
        super(); 
        command = _command;
        this.setAction(new ButtonCommandExecuter(text));
        this.setVerticalTextPosition(AbstractButton.CENTER);
        this.setHorizontalTextPosition(AbstractButton.LEADING); //aka LEFT, for left-to-right locales 
        setBackground(Color.LIGHT_GRAY);
    }
    public class ButtonCommandExecuter extends AbstractAction{        

        private ButtonCommandExecuter(String text) {
            super(text);
        }
        @Override
        public void actionPerformed(ActionEvent e) {
            command.execute();
        }        
    }  
    @Override
    public void setEnabled(boolean value){
        super.setEnabled(value);
        if(value == true){
            setBackground(Color.LIGHT_GRAY);
        }else{
            setBackground(Color.darkGray);
        }
    }
}
