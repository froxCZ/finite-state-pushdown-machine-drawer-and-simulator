/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.union.fsm.gpu.gui;

import java.util.HashSet;

/**
 *
 * @author frox
 */
public class SimulatorMenuPanel extends MenuPanel {
    private final HashSet<Button> playButtons = new HashSet<Button>();
    public SimulatorMenuPanel(int rows_cnt){
        super(rows_cnt);
    }
    
    public void addPlayButton(Button b,int row){
        super.addButton(b, row);
        playButtons.add(b);
    }

    @Override
    public void disableButtons(){
        for(Button b:playButtons){;
            b.setEnabled(false);
        }
    }
    @Override
    public void enableButtons(){
        for(Button b:allButtons){;
            b.setEnabled(true);
        }
    }    

}
