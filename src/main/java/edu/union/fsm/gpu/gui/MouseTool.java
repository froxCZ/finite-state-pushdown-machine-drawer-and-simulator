/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.union.fsm.gpu.gui;

import edu.union.fsm.gpu.FsmControl;
import edu.union.fsm.gpu.helpers.Position;
import edu.union.fsm.gpu.models.Edge;
import edu.union.fsm.gpu.models.FinishType;
import edu.union.fsm.gpu.models.Label;
import edu.union.fsm.gpu.models.Node;
import edu.union.fsm.gpu.models.StartType;
import edu.union.fsm.gpu.models.TemplateObject;
import java.awt.event.KeyEvent;
import javax.swing.JOptionPane;

/**
 *
 * @author frox
 */
public class MouseTool extends EditorTool{
    public String name = "Mouse";
    public String cmd = "mouseTool";
    public MouseTool(Tab _tab) {
        super(_tab);
    }

    @Override
    public void onLeftMouseDown(Position p) {    
        if(!tab.hasFocus()){
            tab.grabFocus();
        }
        TemplateObject o = tab.window.getObjectOnPosition(p);
        if(o == null){
            Window.selectedObject = null;
        }else{
            Window.selectedObject = o;
        }
        System.out.println(FsmControl.selectedObject);
    }
    @Override
    public void onSecondLeftMouseDown(Position p) {
        if(Window.selectedObject == null)return;
        showLabelInputWindow(Window.selectedObject);
    }    
    
    /**
     * shows the label input window and on execution sets the object label 
     * @param o 
     */
    public void showLabelInputWindow(TemplateObject o){
        Label l = o.getLabel();
        l.setRawLabel(JOptionPane.showInputDialog(tab, "Type in label",l.getRawLabel()));
        tab.window.machine.setObjectLabel(o, l);
    }
    
    @Override
    public void onLeftMouseUp(Position p) {
        
    }

    @Override
    public void onLeftMouseDragged(Position p) {
        TemplateObject o = Window.selectedObject;
        if (o == null) {
            return;
        }
        if (o instanceof Edge) {
            tab.window.machine.setEdgeMiddlePoint((Edge) o, p.getX(), p.getY());
        } else if (o instanceof Node) {
            tab.window.machine.setNodePosition((Node) o, p.getX(), p.getY());
        }        
    }
    
    @Override
    public boolean keyPressed(int keyCode){
        if(keyCode == KeyEvent.VK_DELETE && Window.selectedObject != null){
            tab.window.machine.remove(Window.selectedObject);
            return true;
        }
        TemplateObject o = Window.selectedObject;
        if (o == null) return false;
        if (o instanceof Node == false) return false;
        Node n = (Node) o;        
        switch(keyCode){
            case KeyEvent.VK_F:
                tab.window.machine.toggleNodeType(n, new FinishType());
                break;
            case KeyEvent.VK_S:
                tab.window.machine.toggleNodeType(n, new StartType());
                break;
            default:
                return false;
        }                
        return false;
    }
    
    public String getName(){
        return name;
    }
    public String getCmd(){
        return cmd;
    }        
}
