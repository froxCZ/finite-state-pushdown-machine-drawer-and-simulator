/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.union.fsm.gpu.gui;

import java.awt.Color;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JPanel;

/**
 *
 * @author frox
 */
public class EditorMenuPanel extends MenuPanel {

    private SelectToolButton activeToolButton = null;
    protected final HashMap<Class, SelectToolButton> toolButtons;

    public EditorMenuPanel(int rows_cnt) {
        super(rows_cnt);
        this.toolButtons = new HashMap<Class, SelectToolButton>();
    }

    public void addButton(SelectToolButton b, int row) {
        toolButtons.put(b.tool.getClass(), b);
        super.addButton(b, row);
    }

    public void toolSelected(EditorTool tool) {
        if (activeToolButton != null) {
            activeToolButton.deselect();
        }
        activeToolButton = toolButtons.get(tool.getClass());
        activeToolButton.select();
    }

}
