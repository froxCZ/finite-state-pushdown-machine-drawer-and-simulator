package edu.union.fsm.gpu.gui;

import edu.union.fsm.gpu.App;
import edu.union.fsm.gpu.FSMachine;
import edu.union.fsm.gpu.FSSimulator;
import edu.union.fsm.gpu.Machine;
import edu.union.fsm.gpu.PDMachine;
import edu.union.fsm.gpu.PDSimulator;
import edu.union.fsm.gpu.Simulator;
import edu.union.fsm.gpu.gui.commands.Command;
import edu.union.fsm.gpu.gui.commands.ResetSimulatorCommand;
import edu.union.fsm.gpu.models.TemplateObject;
import edu.union.fsm.gpu.helpers.Position;
import edu.union.fsm.gpu.models.FinishType;
import edu.union.fsm.gpu.models.Node;
import edu.union.fsm.gpu.models.StartType;
import edu.union.fsm.gpu.views.display.SelectView;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.AffineTransform;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * Class for the window using Jframe
 *
 * @author frox, ngoodrich, palacet
 */
public class Window implements SimulatorListener {

    public JFrame frame;
    public JTabbedPane panelHolder;
    public EditorTab editorPanel;
    public SimulatorTab simulatorPanel;
    public static Window instance;
    public Simulator simulator;
    public Machine machine;
    public int machineType;

    @Override
    public void simulatorUpdated() {
        simulatorPanel.update();
    }

    public void machineUpdated() {
        editorPanel.update();
    }

    /**
     * creates the window for a specified machine
     *
     * @param m
     */
    public Window() {
        createJFrame();
        setMachineType();
        instance = this;
        Command.setWindow(this);
        //new NewMachineCommand().execute();        
        //initUI();             
        panelHolder = new JTabbedPane();
        editorPanel = new EditorTab(this);        
        panelHolder.add("Editor", editorPanel);
        panelHolder.add("Simulator", simulatorPanel);
        MouseEventHandler mouseHandler = new MouseEventHandler();
        editorPanel.canvas.addMouseListener(mouseHandler);
        editorPanel.canvas.addMouseMotionListener(mouseHandler);

        ChangeListener changeListener = new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent changeEvent) {
                JTabbedPane sourceTabbedPane = (JTabbedPane) changeEvent.getSource();
                int index = sourceTabbedPane.getSelectedIndex();
                if (sourceTabbedPane.getTitleAt(index).equals("Simulator")) {
                    new ResetSimulatorCommand().execute();
                }
            }
        };
        panelHolder.addChangeListener(changeListener);
        frame.add(panelHolder);
        frame.setVisible(true);
        frame.pack();
        this.makeActionMapEditor(editorPanel);
        this.makeActionMapEditor(panelHolder);
    }
    private void makeActionMapEditor(JComponent component) {
        class KeyPressDispatcher extends AbstractAction{
            int keyCode;
            KeyPressDispatcher(int _keyCode){
                keyCode = _keyCode;
            }
            @Override
            public void actionPerformed(ActionEvent e) {
                editorPanel.selectedTool.keyPressed(keyCode);
            }
        };        
        component.getInputMap().put(KeyStroke.getKeyStroke("DELETE"), "deleteKey");
        component.getActionMap().put("deleteKey", new KeyPressDispatcher(KeyEvent.VK_DELETE));
        component.getInputMap().put(KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, 0), "SKey");
        component.getActionMap().put("SKey", new KeyPressDispatcher(KeyEvent.VK_S));
        component.getInputMap().put(KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F, 0), "FKey");
        component.getActionMap().put("FKey", new KeyPressDispatcher(KeyEvent.VK_F));
    }

    /**
     * displays or hides the frame
     *
     * @param visible
     */
    public void setFrameVisible(boolean visible) {
        frame.setVisible(visible);
    }

    /**
     *
     * @return the Jframe frame
     */
    public JFrame getFrame() {
        return frame;
    }

    /**
     * creates the FSM JFrame an canvas
     */
    private void createJFrame() {
        frame = new JFrame("FSM - GPU");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new BorderLayout());
        frame.setResizable(false);
        frame.getContentPane().setBackground(Color.white);
        AffineTransform tx2 = new AffineTransform();
        tx2.scale(.5, .5);
    }

    /**
     * shows file opener and returns the path
     *
     * @return selectedFile
     */
    public String showFileOpenerAndGetPath() {
        //Create a file chooser
        final JFileChooser fc = new JFileChooser();
        //In response to a button click:
        int returnVal = fc.showOpenDialog(frame);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            return fc.getSelectedFile().getAbsolutePath();
        }
        return null;
    }

    /**
     * shows a file saving window for file selection
     *
     * @param extension
     * @return selected file and extention
     */
    public String showFileSaverAndGetPath(String extension) {
        final JFileChooser fc = new JFileChooser();
        if (extension.length() > 0) {
            fc.setFileFilter(new FileNameExtensionFilter(extension, extension));
        }
        int returnVal = fc.showSaveDialog(frame);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            String filePath = fc.getSelectedFile().getAbsolutePath();
            if (filePath.endsWith(extension) == false) {
                filePath += extension;
            }
            return filePath;
        }
        return null;
    }

    public String showInputDialog(String msg, String old) {
        return JOptionPane.showInputDialog(frame, msg, old);
    }

    /**
     * helper for popUp message dialog
     *
     * @param msg
     */
    public void showPopup(String msg) {
        JOptionPane.showMessageDialog(frame, msg);
    }

    /**
     * repaints the frame
     */
    public void repaint() {
        editorPanel.canvas.repaint();
    }

    public static TemplateObject selectedObject; 

    private void setMachineType() {
        String[] options = {"Finite state machine", "Pushdown machine"};
        int n = JOptionPane.showOptionDialog(frame,
                "Select machine type",
                "Select machine type",
                JOptionPane.YES_NO_CANCEL_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null,
                options,
                options[0]);
        if (n == 0) {
            machineType = App.FINITE_MACHINE;
            machine = new FSMachine();
            simulator = new FSSimulator(this, machine);
            simulatorPanel = new SimulatorTab(this);
        } else {
            machineType = App.PUSHDOWN_MACHINE;
            machine = new PDMachine();
            simulator = new PDSimulator(this, machine);            
            simulatorPanel = new PDASimulatorTab(this);
        }
    }
    
    public class MouseEventHandler implements MouseListener, MouseMotionListener {

        int lastX = -1;
        int lastY = -1;
        private Node rightClickSelectedNode;//used for dragging with right click - this is the from node.

        /**
         * When mouse is still pressed but for the first time
         *
         * @param e
         */
        public void onFirstLeftMousePress(MouseEvent e) {
            editorPanel.selectedTool.onLeftMouseDown(new Position(e.getX(), e.getY()));
        }

        /**
         * When mouse is still pressed but for the second time on same place.
         *
         * @param e
         */
        public void onSecondLeftMousePress(MouseEvent e) {
            /*TemplateObject o = selectedObject;
             if (o == null) {
             return;
             }*/
            editorPanel.selectedTool.onSecondLeftMouseDown(new Position(e.getX(), e.getY()));
            //showLabelInputWindow(o);
        }

        /**
         * single and douple click on mouse pressed and held
         *
         * @param e
         */
        @Override
        public void mousePressed(MouseEvent e) {
            if (SwingUtilities.isLeftMouseButton(e)) {
                if (e.getClickCount() == 1) {
                    onFirstLeftMousePress(e);
                } else {
                    onSecondLeftMousePress(e);
                }
            }
        }

        /**
         * on mouse release if mouse moves from one state to another, add
         * corresponding edge
         *
         * @param e
         */
        @Override
        public void mouseReleased(MouseEvent e) {
            if (SwingUtilities.isLeftMouseButton(e)) {
                if(editorPanel.selectedTool!=null){
                    editorPanel.selectedTool.onLeftMouseUp(new Position(e.getX(), e.getY()));
                    repaint();
                }                
            }
        }

        @Override
        public void mouseEntered(MouseEvent e) {
            ;
        }

        @Override
        public void mouseExited(MouseEvent e) {
            ;
        }

        /**
         * on mouse drag either change middle point of edge or change node
         * location depending on what object is selected
         *
         * @param e
         */
        @Override
        public void mouseDragged(MouseEvent e) {
            if (SwingUtilities.isLeftMouseButton(e)) {
                if(editorPanel.selectedTool!=null){
                    editorPanel.selectedTool.onLeftMouseDragged(new Position(e.getX(), e.getY()));
                }
            }
        }

        @Override
        public void mouseMoved(MouseEvent e) {
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

    }

    /**
     * helper to get selected object
     *
     * @return selectedObject
     */
    public static TemplateObject getSelectedObject() {
        return selectedObject;
    }

    /**
     * finds and returns the object on the specified position
     *
     * @param p
     * @return selectView
     */
    public TemplateObject getObjectOnPosition(Position p) {
        selectedObject = SelectView.findObjectOnPosition(machine, p);
        return SelectView.findObjectOnPosition(machine, p);
    }
}
