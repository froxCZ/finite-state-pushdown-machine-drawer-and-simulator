/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.union.fsm.gpu.gui;

import java.awt.BorderLayout;
import javax.swing.JPanel;

/**
 *
 * @author frox
 */
public abstract class Tab extends JPanel{
    public MenuPanel menuPanel;
    public Canvas canvas;
    public Window window;
    public Tab(Window w){        
        super(new BorderLayout());
        window = w;
    }
    public void addMenu(MenuPanel menu){
        menuPanel = menu;
        super.add(menuPanel,BorderLayout.PAGE_START);
    }
    public void addCanvas(Canvas c){
        canvas = c;
        super.add(canvas,BorderLayout.PAGE_END);
    }    
    public abstract void update();
    
}
