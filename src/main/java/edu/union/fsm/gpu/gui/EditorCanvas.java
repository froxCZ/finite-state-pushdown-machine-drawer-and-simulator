/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.union.fsm.gpu.gui;

import edu.union.fsm.gpu.views.display.DisplayView;
import java.awt.Graphics;

/**
 *
 * @author frox
 */
public class EditorCanvas extends Canvas {

    public EditorCanvas(Window w) {
        super(w);        
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        DisplayView.draw(g, window.machine);  
    }

}
