/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.union.fsm.gpu.gui;

import java.awt.Dimension;
import java.awt.Graphics;
import javax.swing.JComponent;

/**
 *
 * @author frox
 */
public class Canvas extends JComponent {

    /**
     * given a graphics object g draws the component
     *
     * @param g
     */
    Window window;

    public Canvas(Window w) {
        super();
        window = w;
        this.setPreferredSize(new Dimension(600, 700));
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.clearRect(0,0,getWidth(),getHeight());
    }
}
