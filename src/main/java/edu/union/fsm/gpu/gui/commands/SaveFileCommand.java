/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.union.fsm.gpu.gui.commands;

import edu.union.fsm.gpu.App;
import edu.union.fsm.gpu.views.Export.SaveView;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author frox
 */
public class SaveFileCommand extends Command {

    @Override
    public void execute() {
        FileWriter w;
        try {
            String filePath = window.showFileSaverAndGetPath(App.EXTENSION);
            if (filePath == null) {
                return;
            }
            w = new FileWriter(filePath);
            SaveView.save(window.machine, w);
            window.showPopup("File saved.");
        } catch (IOException ex) {
            window.showPopup("Error. File was not saved");
        }
    }

}
