/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.union.fsm.gpu.gui.commands;

import edu.union.fsm.gpu.Simulator;
import edu.union.fsm.gpu.gui.Window;

/**
 *
 * @author frox
 */
public class OneStepSimulatorCommand extends SimulatorCommand{

    
    @Override
    public void execute(){
        Window.instance.simulator.doStep();
    }
    
}
