/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.union.fsm.gpu.gui;

import edu.union.fsm.gpu.views.display.SimulatorView;
import java.awt.Graphics;

/**
 *
 * @author frox
 */
public class SimulatorCanvas extends Canvas {

    public SimulatorCanvas(Window w) {
        super(w);
    }

    @Override
    public void paintComponent(Graphics g) {
        SimulatorView.draw(g, window.simulator);  
    }

}
