/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.union.fsm.gpu.gui;

/**
 *
 * @author nicholasgoodrich
 */
public class ScaleTool extends EditorTool{
    public String name = "Scale";
    public String cmd = "scaleCommand";
    public ScaleTool(Tab _tab) {
        super(_tab);
    }
    @Override
    public String getName(){
        return name;
    }
    @Override
    public String getCmd(){
        return cmd;
    }       
    
}
