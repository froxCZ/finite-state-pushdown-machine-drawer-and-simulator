/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.union.fsm.gpu.gui.commands;

import edu.union.fsm.gpu.gui.EditorTab;

/**
 *
 * @author frox
 */
public abstract class EditorCommand extends Command{
    EditorTab editorTab;
    public EditorCommand(EditorTab _editorTab) {
        editorTab = _editorTab;        
    }
    
}
