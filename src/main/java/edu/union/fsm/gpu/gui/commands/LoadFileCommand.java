/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.union.fsm.gpu.gui.commands;

import edu.union.fsm.gpu.gui.Canvas;
import edu.union.fsm.gpu.views.Export.ImportCSV;
import edu.union.fsm.gpu.views.Export.SaveImage;
import edu.union.fsm.gpu.views.Export.SaveView;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import javax.swing.JFrame;

/**
 *
 * @author frox
 */
public class LoadFileCommand extends Command {

    @Override
    public void execute() {
        try {
            String filePath = window.showFileOpenerAndGetPath();
            if (filePath == null) {
                return;
            }
            ImportCSV in = new ImportCSV(filePath, window.machine);
            in.Import();
        } catch (FileNotFoundException ex) {
            window.showPopup("File does not exist!");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        window.machineUpdated();
    }

}
