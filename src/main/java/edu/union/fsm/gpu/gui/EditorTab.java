/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.union.fsm.gpu.gui;

import edu.union.fsm.gpu.gui.commands.*;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JLabel;

/**
 *
 * @author frox
 */
public class EditorTab extends Tab{
    EditorTool selectedTool;
    EditorMenuPanel menuPanel;
    public EditorTab(Window w){
        super(w);
        menuPanel = MenuBuilder.createMenuPanel(this);
        menuPanel.addComponent(new JLabel("Machine type:\t"+window.machine.typeToString()),0);
        canvas = new EditorCanvas(w);
        addMenu(menuPanel);
        addCanvas(canvas);
        this.setBackground(Color.white);
        new SelectToolCommand(this, new MouseTool(this)).execute();
    }

    public void selectTool(EditorTool tool){
        selectedTool = tool;        
        menuPanel.toolSelected(tool);
    }

    @Override
    public void update() {
        canvas.repaint();
    }
}

