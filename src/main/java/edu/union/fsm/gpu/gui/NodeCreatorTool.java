/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.union.fsm.gpu.gui;

import edu.union.fsm.gpu.helpers.Position;
import edu.union.fsm.gpu.models.Node;

/**
 *
 * @author frox
 */
public class NodeCreatorTool extends EditorTool{
    public String name = "Node";
    public String cmd = "nodeTool";    
    public NodeCreatorTool(Tab _tab) {
        super(_tab);
        name = "Node";
    }

    @Override
    public void onLeftMouseDown(Position p) {
        tab.window.machine.addNode(new Node(p));
    }
    
    @Override
    public boolean keyPressed(int keyCode){
        return false;    
    }
    @Override
    public String getName(){
        return name;
    }
    public String getCmd(){
        return cmd;
    }        
    
}
