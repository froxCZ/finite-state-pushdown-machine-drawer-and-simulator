/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.union.fsm.gpu.gui;

import edu.union.fsm.gpu.helpers.Position;
import edu.union.fsm.gpu.models.Node;
import edu.union.fsm.gpu.models.TemplateObject;

/**
 *
 * @author frox
 */
public class EdgeCreatorTool extends EditorTool{
    public String name = "Edge";
    public String cmd = "edgeTool";    
    Node fromNode;
    public EdgeCreatorTool(Tab _tab) {
        super(_tab);
    }

    @Override
    public void onLeftMouseDown(Position p) {
        TemplateObject o = tab.window.getObjectOnPosition(p);
        if(o == null)return;
        if(o instanceof Node == false)return;
        fromNode = (Node)o;        
    }

    @Override
    public void onLeftMouseUp(Position p) {
        TemplateObject o = tab.window.getObjectOnPosition(p);
        if(o == null)return;
        if(o instanceof Node == false)return;
        tab.window.machine._addEdge(fromNode, (Node)o);
    }
    public String getName(){
        return name;
    }
    public String getCmd(){
        return cmd;
    }        
    
}
