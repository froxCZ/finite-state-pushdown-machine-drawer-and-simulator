/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.union.fsm.gpu.gui.commands;

import edu.union.fsm.gpu.gui.*;

/**
 *
 * @author frox
 */
public abstract class Command {
    public static Window window;
    public static void setWindow(Window w){
        window = w;
    }
    public abstract void execute();
}
