/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.union.fsm.gpu.gui.commands;

import java.awt.Dimension;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author frox
 */
public class ChangeCanvasSizeCommand extends Command{

    @Override
    public void execute(){
        JTextField widthString = new JTextField();
        JTextField heightString = new JTextField();
        Object[] message = {
            "Width:", widthString,
            "Height:", heightString
        };

        int option = JOptionPane.showConfirmDialog(window.frame, message, "Canvas Size", JOptionPane.OK_CANCEL_OPTION);
        if (option == JOptionPane.OK_OPTION) {
            try{
                int width = Integer.valueOf(widthString.getText());
                int height = Integer.valueOf(heightString.getText());
                window.editorPanel.canvas.setPreferredSize(new Dimension(width, height));
                window.simulatorPanel.canvas.setPreferredSize(new Dimension(width, height));
                window.frame.pack();
            }catch(NumberFormatException e){
                ;
            }

        }
    }
}
