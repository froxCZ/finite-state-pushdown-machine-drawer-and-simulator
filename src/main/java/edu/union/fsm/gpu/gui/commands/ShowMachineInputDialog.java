/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.union.fsm.gpu.gui.commands;

import edu.union.fsm.gpu.Simulator;
import edu.union.fsm.gpu.gui.Window;

/**
 *
 * @author frox
 */
public class ShowMachineInputDialog extends SimulatorCommand{

    
    @Override
    public void execute(){
        String newInput = window.showInputDialog("Enter machine input", window.simulator.input);
        if(newInput==null)return;
        window.simulator.input = newInput;
        new ResetSimulatorCommand().execute();
    }
    
}
