/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.union.fsm.gpu.gui.commands;

import edu.union.fsm.gpu.App;
import edu.union.fsm.gpu.views.Export.SaveView;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author frox
 */
public class SaveLatexCommand extends Command {

    @Override
    public void execute() {
        FileWriter w;
        try {
            String filePath = window.showFileSaverAndGetPath(App.LATEX_EXTENSION);
            if (filePath == null) {
                return;
            }
            w = new FileWriter(filePath);
            SaveView.saveLatex(window.machine, w);
            window.showPopup("File saved.");
        } catch (Exception ex) {
            window.showPopup("Error. File was not saved. Message: "+ex.getMessage());
        }
    }

}
