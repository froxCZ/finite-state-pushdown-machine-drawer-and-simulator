/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.union.fsm.gpu.gui;

import edu.union.fsm.gpu.PDSimulator;
import edu.union.fsm.gpu.gui.commands.ShowMachineInputDialog;
import edu.union.fsm.gpu.gui.commands.ShowMachineStackInputDialog;
import javax.swing.JLabel;

/**
 *
 * @author frox
 */
public class PDASimulatorTab extends SimulatorTab{
    JLabel stackJLabel = new JLabel();
    PDSimulator simulator;
    public PDASimulatorTab(Window w){
        super(w);
        simulator = (PDSimulator)window.simulator;
        menuPanel.addComponent(new JLabel("Stack:\t"),1);
        menuPanel.addComponent(stackJLabel, 1);   
        menuPanel.addButton(new Button("Set stack",new ShowMachineStackInputDialog()),2);
    }    
    @Override
    public void update() {
        super.update();
        stackJLabel.setText(simulator.stack);      
    }    
}
