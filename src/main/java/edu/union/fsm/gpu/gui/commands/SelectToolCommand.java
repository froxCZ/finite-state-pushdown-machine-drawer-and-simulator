/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.union.fsm.gpu.gui.commands;

import edu.union.fsm.gpu.gui.*;

/**
 *
 * @author frox
 */
public class SelectToolCommand extends EditorCommand{
    EditorTool tool;
    public SelectToolCommand(EditorTab tab,EditorTool _tool) {
        super(tab);
        tool = _tool;
    }
    @Override
    public void execute(){
        editorTab.selectTool(tool);
    }
}
