/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.union.fsm.gpu.gui.commands;

import edu.union.fsm.gpu.Simulator;
import edu.union.fsm.gpu.gui.InvalidMachineSetupException;
import edu.union.fsm.gpu.gui.Window;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author frox
 */
public class ResetSimulatorCommand extends SimulatorCommand{
    public ResetSimulatorCommand() {
        super();
    }    
    
    @Override
    public void execute(){
        window.simulator.reset(Window.instance.machine.getStartNodes(),window.machine.getFinishNodes(), window.simulator.input);
        try {
            window.simulator.checkMachineValidity();
            window.simulatorPanel.menuPanel.enableButtons();
            window.simulatorPanel.setReady();
        } catch (InvalidMachineSetupException ex) {
            window.simulatorPanel.menuPanel.disableButtons();
            window.simulatorPanel.setErrorMsg(ex.getMessage());
        }        
    }
    
}
