
package edu.union.fsm.gpu.models;

import edu.union.fsm.gpu.Machine;
import edu.union.fsm.gpu.helpers.Position;
import java.awt.Color;

/**
 * An template for all objects of the FSM
 * @author frox, ngoodrich, palacet
 */
public abstract class TemplateObject implements Printable,Selectable{
    static int CLICK_PRECISION=10;
    private Color color;
    protected Label label;
    private String hash;
    private boolean selected = false;    
    protected Machine fsm;
    /**
     * default constructor for template object
     */
    TemplateObject(){
        hash = Long.toHexString(Double.doubleToLongBits(Math.random()));
        label = new Label();
    }   
    /**
     * 
     * @param p
     * @return false does not matter as it is overridden in both node and edge
     */
    public boolean isOnPosition(Position p)
    {        
        return false;
    }
    /**
     * 
     * @param f fsm to set for the templateobject
     */
    public void setFsm(Machine f){
        fsm = f;
    }
    /**
     * sets selected to true
     */
    public void selected(){
        selected = true;
    }
    /**
     * sets selected to false
     */
    public void deselected(){
        selected = false;
    }
    /**
     * returns whether or not the template object is selected
     * @return selected
     */
    public boolean isSelected(){
        return selected;
    }
    /**
     * set a new string hash for the template object
     * @param newHash 
     */
    public void setHash(String newHash){
        this.hash = newHash;
    }
    /**
     * 
     * @return string hashcode of template object
     */
    @Override
    public int hashCode() {    
        return hash.hashCode();
    }
    /**
     * @return the color
     */
    public Color getColor() {
        return color;
    }

    /**
     * @param color the color to set
     */
    public void setColor(Color color) {
        this.color = color;
    }

    /**
     * @return the label
     */
    public Label getLabel() {
        return label;
    }

    /**
     * @param l
     */
    public void setLabel(Label l) {
        this.label = l;
    }
    public abstract void remove();   
    public abstract int getPriority();
}
