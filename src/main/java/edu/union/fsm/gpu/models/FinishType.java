

package edu.union.fsm.gpu.models;

import edu.union.fsm.gpu.views.display.NodeTypeVisitor;

/**
 * finish type for Node
 * @author frox
 */
public class FinishType extends NodeType{

   // @Override
    public void accept(NodeTypeVisitor v) {
        v.accept(this);
    }
    @Override
    public boolean equals(Object aThat) {
        if(aThat==null)return false;
        if ( this == aThat ) return true;
        if ( !(aThat instanceof FinishType) ) return false;
        return true;
    }    
}
