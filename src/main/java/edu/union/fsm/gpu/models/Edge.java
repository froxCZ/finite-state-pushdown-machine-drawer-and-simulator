

package edu.union.fsm.gpu.models;

import edu.union.fsm.gpu.helpers.Position;
import edu.union.fsm.gpu.views.Export.LatexVisitor;
import edu.union.fsm.gpu.views.PrintVisitor;
import edu.union.fsm.gpu.views.display.SelectVisitor;

import java.awt.geom.Path2D;

/**
 *Model class for Edge (aka transition) extends TemplateObject
 * @author frox, palacet, ngoodrich
 */
public class Edge extends TemplateObject{
    private final Node fromNode;
    private final Node toNode;
    private final Position lineMiddlePoint;
    protected final int PRIORITY = 100;   
    /**
     * non-default constructor
     * @param from state of transition
     * @param to state of transition
     */
    public Edge(Node from, Node to) {
        super();
        fromNode = from;
        toNode = to;        
        Position fromPos = fromNode.getPosition();
        Position toPos = toNode.getPosition();
        if(fromNode.equals(toNode)){
         lineMiddlePoint = new Position(fromPos.getX(), fromPos.getY()+100);   
        }
        else{
        lineMiddlePoint = new Position((fromPos.getX()+toPos.getX())/2,(fromPos.getY() + toPos.getY())/2);
        }
    }
    /**
     * asks if position lies on edge
     * @param p
     * @return false
     */
    @Override
    public boolean isOnPosition(Position p)
    {        
        return false;
    }

    /**
     * @return the sourceNode
     */
    public Node getFromNode() {
        return fromNode;
    }

    /**
     * @return the destinationNode
     */
    public Node getToNode() {
        return toNode;
    }    

    /**
     * sets the middle point of the line for the line bend
     * @param x
     * @param y 
     */
    public void setLineMiddlePoint(int x, int y){
        getLineMiddlePoint().setX(x);
        getLineMiddlePoint().setY(y);   
    }

    /**
     * @return the lineMiddlePoint
     */
    public Position getLineMiddlePoint() {
        return lineMiddlePoint;
    }
    /**
     * removes the edge from the from and to Node's adjacency list
     */
    @Override
    public void remove() {
        fromNode.removeEdge(this);
        toNode.removeEdge(this);
    }
    /**
     * 
     * @return priority value of this object
     */
    @Override
    public int getPriority() {
        return PRIORITY;
    }
    /**
     * visits this edge via printvisitor
     * @param v printvisitor 
     */
    @Override
    public void accept(PrintVisitor v) {
        v.visit(this);
    }
    public void accept(LatexVisitor v){
        v.visit(this);
    }
    /**
     * 
     * @param v
     * @return the result of the visit of selectvisitor
     */
    @Override
    public boolean accept(SelectVisitor v) {
        return v.visit(this);
    }
    /**
     * 
     * @return string representation of this edge 
     * in form "Position:middlepoint","FromNode:fromNode","ToNode:toNode"
     */
    @Override
    public String toString(){
        String toReturn = "";
        toReturn = toReturn + "Position:" + this.getLineMiddlePoint().toString();
        toReturn = toReturn + "FromNode:" + this.getFromNode().toString();
        toReturn = toReturn + "ToNode:" + this.getToNode().toString();
        return toReturn;
    }

}
