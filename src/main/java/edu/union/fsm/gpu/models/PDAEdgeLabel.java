

package edu.union.fsm.gpu.models;

import edu.union.fsm.gpu.helpers.SymbolDictionary;
import edu.union.fsm.gpu.views.PrintVisitor;

/**
 *
 * @author frox,palacet,ngoodrich
 */
public class PDAEdgeLabel extends Label{
    protected final String onStack = ";";
    protected final String pushStack = "/";
            
    public PDAEdgeLabel(){
        super();
        System.out.println("PDAEDGELABEL");
    }
    public PDAEdgeLabel(String raw){
        super(raw);
    }    
    
    public String getMustBeOnStack(){
        String l = this.getLabel(); 
        if(l.indexOf(onStack) > -1){
            return l.substring(l.indexOf(onStack)+1,l.indexOf(onStack)+2);
        }
        return null;
    }
    public String getPushOnStack(){
        String l = this.getLabel(); 
        if(l.indexOf(pushStack) > -1){
            return l.substring(l.indexOf(pushStack)+1);
        }
        return null;
    }
    
    public String getMustBeOnInput(){
        return this.getLabel().substring(0,1);
    }
    
}
