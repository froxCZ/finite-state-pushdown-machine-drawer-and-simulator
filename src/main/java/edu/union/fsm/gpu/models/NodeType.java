

package edu.union.fsm.gpu.models;

import edu.union.fsm.gpu.views.display.NodeTypeVisitor;

/**
 *
 * @author frox
 */
public abstract class NodeType {
    public abstract void accept(NodeTypeVisitor t);
    public abstract boolean equals(Object o);
}
