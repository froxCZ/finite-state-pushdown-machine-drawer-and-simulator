

package edu.union.fsm.gpu.models;

import edu.union.fsm.gpu.views.Visitor;
import edu.union.fsm.gpu.views.display.SelectVisitor;

/**
 *
 * @author frox
 */
public interface Selectable {
    public boolean accept(SelectVisitor v);
}
