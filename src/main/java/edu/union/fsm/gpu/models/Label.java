

package edu.union.fsm.gpu.models;

import edu.union.fsm.gpu.helpers.SymbolDictionary;
import edu.union.fsm.gpu.views.PrintVisitor;

/**
 *
 * @author frox,palacet,ngoodrich
 */
public class Label implements Printable{
    protected String rawLabel;
    protected String label;
    protected String subscript;
    protected String exponent;
    protected final String subSym = "_";
    protected final String expSym = "^";
    protected final String beginSym = "{";
    protected final String endSym = "}";
    protected final SymbolDictionary dictionary;
    
    /**
     * Default Constructor
     */
    public Label(){
        rawLabel ="";
        label = "";
        subscript = "";
        exponent = "";
        dictionary = new SymbolDictionary();
    }
    
    /**
     * No Default Constructor
     * @param raw, the raw text that is to be put into the label
     */
    public Label(String raw){        
        label ="";
        subscript = "";
        exponent = "";
        dictionary = new SymbolDictionary();
        setRawLabel(raw);        
    }
    
    /**
     * Sets the raw label 
     * @param l, the raw label that is to be set
     */
    public void setRawLabel(String l)
    {
        if(l==null){return;}
        this.rawLabel = l;
   
     
        this.updateExponent(); 
        this.updateSubscript();
        this.updateLabel();
        this.updateSymbols();
     
    }
    
    /**
     * @return the rawLabel (the input that was put in originally)
     */
    public String getRawLabel()
    {
        return this.rawLabel;
    }
    /**
     * @return the label
     */
    public String getLabel() {
        return label;
    }

    /**
     * 
     * @return true if there is a subscript contained within the label
     */
    public boolean hasSubscriptRaw(){
        String l = this.getRawLabel();
        int index =  l.indexOf(subSym);
        return (index > -1);
    }
    
    
    /**
     * 
     * @return true if a subscript has been specified or if there is a subscript within the label
     */
    public boolean hasSubscript(){
        return (!this.getSubscript().equals("") || this.hasSubscriptRaw());
    }

    /**
     * 
     * @return true if there is an exponent within the label 
     */
    public boolean hasExponentRaw(){
        String l = this.getRawLabel();
        int index = l.indexOf(expSym);
        return (index > -1);
    }
    
    /**
     * 
     * @return true if the label contains an exponent or if an exponent has already been set 
     */
    public boolean hasExponent(){
        return (!this.getExponent().equals("") || this.hasExponentRaw());
    }
    
    /**
     * 
     * @param l the string that is in question for having a symbol
     * @return true if this string contains { } to denote that this is a symbol
     */
    public boolean hasSymbol(String l){
        int begin = l.indexOf(beginSym);
        int end = l.indexOf(endSym);
        return (end > begin && begin > -1 && end > -1);
    }
    
    
    
    /**
     * Checks to see if the rawLabel has any symbols
     * @return true if it contains symbols
     */
    public boolean hasSymbol(){
        String l = this.getRawLabel();
        int begin = l.indexOf(beginSym);
        int end = l.indexOf(endSym);
        return (end > begin && begin > -1 && end > -1);
    }
    
    /**
     * @param label the label to set
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * @return the subscript
     */
    public String getSubscript() {
        return subscript;
    }

    /**
     * @param subscript the subscript to set
     */
    public void setSubscript(String subscript) {
        this.subscript = subscript;
    }

    /**
     * @return the exponent
     */
    public String getExponent() {
        return exponent;
    }

    /**
     * @param exponent the exponent to set
     */
    public void setExponent(String exponent) {
        this.exponent = exponent;
    }
    
    /**
     * private helper method that takes the rawLabel and extracts the subscript
     * and sets the instance variable subscript to this value
     */
    private void updateSubscript(){
        if(this.hasSubscriptRaw()){
            int subIndex = this.getRawLabel().indexOf(subSym);
            int expIndex = this.getRawLabel().indexOf(expSym);
            if(subIndex < this.getRawLabel().length()){
                if(subIndex < expIndex){
                    this.setSubscript(this.getRawLabel().substring(subIndex+1,expIndex));
                }
                else if (subIndex > expIndex){
                    this.setSubscript(this.getRawLabel().substring(subIndex + 1));
                }
                else{
                    this.setSubscript("");
                }
            }
        }
        else{
            this.setSubscript("");
        }
    }
    
    /**
     * private helper method that takes the rawLabel and extracts the exponent
     * and sets the instance variable exponent to this value
     */
    private void updateExponent(){
        if(this.hasExponentRaw()){
            int subIndex = this.getRawLabel().indexOf(subSym);
            int expIndex = this.getRawLabel().indexOf(expSym);
            if(expIndex < this.getRawLabel().length()){
                if(expIndex < subIndex){
                    this.setExponent(this.getRawLabel().substring(expIndex+1,subIndex));
                }
                else if (expIndex > subIndex){
                    this.setExponent(this.getRawLabel().substring(expIndex+1));
                }
                else{
                    this.setExponent("");
                }
            }
        }
        else{
            this.setExponent("");
        }
    }
    
   /**
    * private helper method that extracts the label from rawLabel
    * and assigns this value to the instance variable label
    */
    private void updateLabel(){
        int subIndex = this.getRawLabel().indexOf(subSym);
        int expIndex = this.getRawLabel().indexOf(expSym);
        
        if(subIndex == -1){
            if(expIndex == -1){
                this.setLabel(this.getRawLabel());
            }
            else{
                this.setLabel(this.getRawLabel().substring(0,expIndex));
            }
        }
        else if (expIndex == -1){
            if(subIndex == -1){
                this.setLabel(this.getRawLabel());
            }
            else{
                this.setLabel(this.getRawLabel().substring(0,subIndex));
            }
        }
        else{
           if(subIndex < expIndex){
               this.setLabel(this.getRawLabel().substring(0,subIndex));
           } 
           else{
               this.setLabel(this.getRawLabel().substring(0,expIndex));
           }
        }
    }
         private void updateSymbols(){
//        while(this.hasSymbol()){
//            String raw = this.getRawLabel();
//            String toReplace = raw.substring(raw.indexOf(beginSym)+1,raw.indexOf(endSym));
//            String uni = dictionary.lookUp(toReplace);
//            String newLabel = raw.substring(0,raw.indexOf("{")) + uni + raw.substring(raw.indexOf("}")+1);
//            this.setRawLabel(newLabel);
//        }
         while(this.hasSymbol(this.getLabel())){
             String oldLabel = this.getLabel();
             String toReplace = oldLabel.substring(oldLabel.indexOf(beginSym)+1,oldLabel.indexOf(endSym));
             String uni = dictionary.lookUp(toReplace);
             String newLabel = oldLabel.substring(0,oldLabel.indexOf("{")) + uni + oldLabel.substring(oldLabel.indexOf("}")+1);
             this.setLabel(newLabel);
         }
         while(this.hasSymbol(this.getSubscript())){
             String oldSub = this.getSubscript();
             String toReplace = oldSub.substring(oldSub.indexOf(beginSym)+1,oldSub.indexOf(endSym));
             String uni = dictionary.lookUp(toReplace);
             String newSub = oldSub.substring(0,oldSub.indexOf("{")) + uni + oldSub.substring(oldSub.indexOf("}")+1);
             this.setSubscript(newSub);
         }
          while(this.hasSymbol(this.getExponent())){
             String oldExp = this.getExponent();
             String toReplace = oldExp.substring(oldExp.indexOf(beginSym)+1,oldExp.indexOf(endSym));
             String uni = dictionary.lookUp(toReplace);
             String newExp = oldExp.substring(0,oldExp.indexOf("{")) + uni + oldExp.substring(oldExp.indexOf("}")+1);
             this.setExponent(newExp);
         }
    }


    @Override
    public void accept(PrintVisitor v) {
        ;
    }
}
