

package edu.union.fsm.gpu.models;

import edu.union.fsm.gpu.views.PrintVisitor;

/**
 *
 * @author frox
 */
public interface Printable {
    public void accept(PrintVisitor v);
}
