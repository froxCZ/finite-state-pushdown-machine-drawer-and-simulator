package edu.union.fsm.gpu.models;

import edu.union.fsm.gpu.views.PrintVisitor;
import edu.union.fsm.gpu.views.display.NodeTypeVisitor;

/**
 *
 * @author frox
 */
public class StartType extends NodeType{
   
    @Override
    public void accept(NodeTypeVisitor v) {
        v.accept(this);
    }

    @Override
    public boolean equals(Object aThat) {
        if(aThat==null)return false;
        if ( this == aThat ) return true;
        if ( !(aThat instanceof StartType) ) return false;
        return true;
    }

    
}