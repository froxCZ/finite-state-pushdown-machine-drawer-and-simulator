

package edu.union.fsm.gpu.models;

import edu.union.fsm.gpu.models.*;
import edu.union.fsm.gpu.helpers.Position;
import edu.union.fsm.gpu.views.Export.LatexVisitor;
import edu.union.fsm.gpu.views.display.NodeView;
import edu.union.fsm.gpu.views.PrintVisitor;
import edu.union.fsm.gpu.views.display.SelectVisitor;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

/**
 * Node object, a type of TemplateObject
 * @author frox
 */
public class Node extends TemplateObject{
    private List<NodeType> type;
    private List<Edge> outEdges;
    private List<Edge> inEdges;
    private Position position;
    protected final int PRIORITY = 1000;
    public Node(Position p){
        super();
        position = p;        
        outEdges = new ArrayList<Edge>();
        inEdges = new ArrayList<Edge>();
        type = new ArrayList<NodeType>();
    }
      
    /**
     * adds out edge to the list of out edges
     * @param toAdd 
     */
    public void addOutEdge(Edge toAdd){
        outEdges.add(toAdd);
    }
    /**
     * adds in edge to list of in edges
     * @param toAdd 
     */
    public void addInEdge(Edge toAdd){
        inEdges.add(toAdd);
    }
    /**
     * removes an edge from the two edgeLists
     * @param toRemove 
     */
    public void removeEdge(Edge toRemove){
        outEdges.remove(toRemove);
        inEdges.remove(toRemove);
    } 
    
     /**
     * @param type the type to set
     */
    public void addType(NodeType type) {
        this.type.add(type);
    }
    /**
     * removes specified type from nodeTypes
     * @param type 
     */
    public void removeType(NodeType type){
        this.type.remove(type);
    }
    public void toggleNodeType(NodeType t){
        if (this.type.contains(t)) {
            type.remove(t);
        } else {
            type.add(t);
        }
    }
    /**
     * 
     * @return true if t is of finishtype
     */
    public boolean hasFinishType(){
        Iterator<NodeType> iter = this.getType().iterator();
        while(iter.hasNext()){
            NodeType t = iter.next();
            if(t instanceof FinishType){
                return true;
            }
        }
        return false;
    }
    /**
     * 
     * @return true if node is of startType
     */
    public boolean hasStartType(){
        Iterator<NodeType> iter = this.getType().iterator();
        while(iter.hasNext()){
            NodeType t = iter.next();
            if(t instanceof StartType){
                return true;
            }
        }
        return false;
    }
    
    
    /**
     * @param Position p
     * @return true if the the position is in this node
     */
    @Override
    public boolean isOnPosition(Position p){
        double xRadius = NodeView.RADIUS;
        double yRadius = NodeView.RADIUS;
        double distance = Math.hypot((double)(p.getX()-this.position.getX()), (double)(p.getY()-this.position.getY()));
   
        return (distance <= xRadius && distance <= yRadius);
    }
    
    
    /**
     * 
     * @return position of node
     */
    public Position getPosition(){
        return position;
    }

    /**
     * @return the type iterable
     */
    public Iterable getType() {
        return type;
    }

   

    /**
     * @return the outEdges list
     */
    public List<Edge> getOutEdges() {
        return outEdges;
    }
    /**
     * @return the inEdges list
     */
    public List<Edge> getInEdges() {
        return inEdges;
    }    
    
    /**
     * @param position the position to set
     */
    public void setPosition(Position position) {
        this.position = position;      
    }
    /**
     * removes the node from the fsm
     */
    @Override
    public void remove() {
       List<Edge> toRemove = new ArrayList<Edge>(outEdges);//to prevent list collision
       toRemove.addAll(inEdges);
       for(Edge e:toRemove){
           fsm.remove(e);
       }
    }
    /**
     * 
     * @return priority  
     */
    @Override
    public int getPriority() {
        return PRIORITY;
    }
    /**
     * 
     * @param v 
     */
    @Override
    public void accept(PrintVisitor v) {
        v.visit(this);
    }
    /**
     * 
     * @param v
     * @return 
     */
    @Override
    public boolean accept(SelectVisitor v) {
        return v.visit(this);
    }   
    
    public void accept(LatexVisitor v) {
         v.visit(this);
    }  
    
//    @Override
 //   public String toString(){
//        String toReturn = "";
//        toReturn = toReturn + "NodeType:" + this.type.toString();
//        toReturn = toReturn + "Position:" + this.position.toString();
//        return toReturn;
//    }
    
}
