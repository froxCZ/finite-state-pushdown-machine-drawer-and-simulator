
package edu.union.fsm.gpu;

import edu.union.fsm.gpu.gui.InvalidMachineSetupException;
import edu.union.fsm.gpu.models.Label;
import edu.union.fsm.gpu.helpers.Position;
import edu.union.fsm.gpu.models.Edge;
import edu.union.fsm.gpu.models.FinishType;
import edu.union.fsm.gpu.models.Node;
import edu.union.fsm.gpu.models.StartType;
import edu.union.fsm.gpu.models.TemplateObject;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.TreeSet;
import java.util.Vector;

/**
 * This class is the model for the finite state machine, it contains the states and transitions
 * @author frox, ngoodrich, palacet
 */
public class FSMachine extends Machine{
    public FSMachine(){
        super();
    }    

    @Override
    public String typeToString() {
        return "FS Machine";
    }

}
