
package edu.union.fsm.gpu;

import edu.union.fsm.gpu.models.Label;
import edu.union.fsm.gpu.helpers.Position;
import edu.union.fsm.gpu.models.Edge;
import edu.union.fsm.gpu.models.FinishType;
import edu.union.fsm.gpu.models.Node;
import edu.union.fsm.gpu.models.PDAEdgeLabel;
import edu.union.fsm.gpu.models.StartType;
import edu.union.fsm.gpu.models.TemplateObject;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.TreeSet;
import java.util.Vector;

/**
 * This class is the model for the finite state machine, it contains the states and transitions
 * @author frox, ngoodrich, palacet
 */
public class PDMachine extends Machine{
    public PDMachine(){
        super();
    }  
    public void _addEdge(Node from,Node to){
        _addEdge(from,to,new PDAEdgeLabel());
    }
    @Override
    public String typeToString() {
        return "PDA Machine";
    }

}
