/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.union.fsm.gpu;

import edu.union.fsm.gpu.gui.*;
import edu.union.fsm.gpu.gui.SimulatorListener;
import edu.union.fsm.gpu.models.Edge;
import edu.union.fsm.gpu.models.Node;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

/**
 *
 * @author frox
 */
public class FSSimulator extends Simulator{

    public FSSimulator(SimulatorListener _listener, Machine _fsm) {
        super(_listener, _fsm);
    }
    @Override
    public void reset(Collection _startStates,Collection _finishStates,String _input){
        super.reset(_startStates, _finishStates, _input);
        moveOverEpsilonEdges();        
    }
    @Override
    public void doStep() {
        HashSet<Node> newAtStates = new HashSet<Node>();
        for(Node n:atStates){
            for(Edge e: n.getOutEdges()){
                if(e.getLabel().getLabel().charAt(0) == input.charAt(inputPointer)){
                    newAtStates.add(e.getToNode());
                }
            }
        }
        inputPointer++;
        atStates = newAtStates;
        moveOverEpsilonEdges();        
        invokeListeners();
    }
    public void moveOverEpsilonEdges(){
        HashSet<Node> alsoAtStates = new HashSet<Node>(atStates);
        for(Node n:atStates){
            for(Edge e: n.getOutEdges()){
                if(e.getLabel().getRawLabel().equals("{epsilon}")){
                    alsoAtStates.add(e.getToNode());
                }
            }
        }
        if(alsoAtStates.size() > atStates.size()){
            atStates = alsoAtStates;
            moveOverEpsilonEdges();
        }        
    }     
    @Override
    public void checkMachineValidity() throws InvalidMachineSetupException{
        super.checkMachineValidity();
        checkLabelLength();
    }    
    private boolean checkLabelLength() throws InvalidMachineSetupException {
        for(Edge e:machine.getEdges()){
            if(e.getLabel() == null)throw new InvalidMachineSetupException("some edges does not have label!");
            if(e.getLabel().getLabel().length() > 1)throw new InvalidMachineSetupException("some edges have label longer than 1");
        }
        return true;
    }        

}
