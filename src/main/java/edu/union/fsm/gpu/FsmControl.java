
package edu.union.fsm.gpu;

import edu.union.fsm.gpu.gui.*;
import edu.union.fsm.gpu.helpers.Position;
import edu.union.fsm.gpu.models.Edge;
import edu.union.fsm.gpu.models.FinishType;
import edu.union.fsm.gpu.models.Node;
import edu.union.fsm.gpu.models.StartType;
import edu.union.fsm.gpu.models.TemplateObject;
import edu.union.fsm.gpu.views.Export.ImportCSV;
import edu.union.fsm.gpu.views.Export.SaveImage;
import edu.union.fsm.gpu.views.Export.SaveView;
import edu.union.fsm.gpu.views.display.SelectView;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
/**
 * This class is the controller for the finite state machine model
 * @author frox, ngoodrich, palacet
 */
public class FsmControl implements FsmListener{
    private Machine fsm;
    private Window window;
    public static TemplateObject selectedObject;
    /**
     * Non-default constructor, sets the window and finite state machines also instantiates the listeners
     * @param w
     * @param f 
     */
    public FsmControl(Window w,Machine f) {
        window = w;
        fsm = f;
        fsm.setControlListener(this);
    }      
    /**
     * updates and repaints the Window, overrides update from fsmListener
     */
    @Override
    public void update() {
        window.machineUpdated();
    }
    /**
     * Clears the current fsm and opens a file opening window to select a file to load
     * loads fsm from csv file.
     */
    private void loadFile() {
        try {
            String filePath = window.showFileOpenerAndGetPath();
            if (filePath == null) {
                return;
            }
            ImportCSV in = new ImportCSV(filePath, fsm);
            in.Import();
        } catch (FileNotFoundException ex) {
            window.showPopup("File does not exist!");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        update();
    }
    /**
     * opens a file selecting window for save location and saves the fsm to a csv file
     */



}
