/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.union.fsm.gpu.tests;

import edu.union.fsm.gpu.helpers.Position;
import static junit.framework.Assert.assertEquals;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

/**
 *
 * @author nicholasgoodrich
 */
@RunWith(JUnit4.class)
public class PositionTests {
    Position p1;
    
    
    @Before
    public void setUp()
    {
         p1 = new Position(1,1);
    }

    @After
    public void tearDown()
    {
        p1 = null;
    }
    @Test
    public void getPositionTests(){
    assertEquals("getX() returns incorrect value",1,p1.getX());
    assertEquals("getY() returns incorrect value",1,p1.getX());
    }
    @Test
    public void setPositionTests(){
        p1.setX(2);
        assertEquals("setX() does not set proper x value",2,p1.getX());
        p1.setY(2);
        assertEquals("setY() does not set proper x value",2,p1.getY());
    }
}
