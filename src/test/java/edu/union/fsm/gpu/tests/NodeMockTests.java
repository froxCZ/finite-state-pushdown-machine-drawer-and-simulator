/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.union.fsm.gpu.tests;

import edu.union.fsm.gpu.FSMachine;
import edu.union.fsm.gpu.Machine;
import edu.union.fsm.gpu.FsmControl;
import edu.union.fsm.gpu.gui.Window;
import edu.union.fsm.gpu.models.*;
import edu.union.fsm.gpu.helpers.*;
import edu.union.fsm.gpu.views.PrintVisitor;
import org.junit.Before;
import static org.junit.Assert.*;
import org.junit.Test;
/**
 *
 * @author frox
 */
public class NodeMockTests {
    Machine fsm;
    MockNode n;
    @Before
    public void setUp()
    {
        Window window = new Window();      
        fsm = window.machine;
        window.setFrameVisible(false);
        FsmControl fsmControl = new FsmControl(window,fsm);        
        n = new MockNode(new Position(1,2));
    }       
    @Test
    public void testEdgeAddingAndRemoving(){
        fsm._addEdge(n, new Node(new Position(3, 5)));
        n.verify(0, 1, 0);
        fsm._addEdge(new Node(new Position(3, 3)), n);
        n.verify(1, 1, 0);
        Node n2 = new Node(new Position(6, 3));
        fsm.addNode(n2);
        fsm._addEdge(n2, n);
        n.verify(2, 1, 0);
        fsm.remove(n2);
        n.verify(2, 1, 1);
        
    }
    
    private class MockNode extends Node{ 
        public int addInEdgeCnt;        
        public int addOutEdgeCnt;
        public int removeEdgeCnt;

        public MockNode(Position p) {
            super(p);
        }
        @Override
        public void addInEdge(Edge toAdd){
            super.addInEdge(toAdd);
            addInEdgeCnt++;
        }          
        @Override
        public void addOutEdge(Edge toAdd){
            super.addOutEdge(toAdd);
            addOutEdgeCnt++;
        }
        @Override
        public void removeEdge(Edge toRemove){        
            super.removeEdge(toRemove);
            removeEdgeCnt++;
        }
        /* Has never been called, why??
        @Override
        public void accept(PrintVisitor v) {
            super.accept(v);
            acceptPrintCnt++;
        } */       
        public void verify(int in,int out,int remove){
            assertEquals("Wrong number of addInEdge() calls",in, addInEdgeCnt);
            assertEquals("Wrong number of addOutEdge() calls",out, addOutEdgeCnt);
            assertEquals("Wrong number of removeEdge() calls",remove, removeEdgeCnt);
        }
    }
}
