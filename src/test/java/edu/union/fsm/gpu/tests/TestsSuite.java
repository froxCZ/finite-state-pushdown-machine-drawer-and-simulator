package edu.union.fsm.gpu.tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import edu.union.fsm.gpu.*;

@RunWith(Suite.class)
@Suite.SuiteClasses
({    
    /*SimpleTest.class, 
    LabelTests.class,
    NodeMockTests.class,
    FsmUpdateTest.class*/
})
public class TestsSuite
{ // no implementation needed; above annotations do the work.
}