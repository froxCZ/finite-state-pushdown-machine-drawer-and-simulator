/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.union.fsm.gpu.tests;


import edu.union.fsm.gpu.*;
import edu.union.fsm.gpu.gui.Window;
import edu.union.fsm.gpu.helpers.Position;
import edu.union.fsm.gpu.models.FinishType;
import edu.union.fsm.gpu.models.Node;
import edu.union.fsm.gpu.models.StartType;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;

import org.junit.Test;
import org.junit.Before;
import org.junit.After;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import javax.management.RuntimeErrorException;
import org.junit.Rule;
import org.junit.rules.ExpectedException;

/**
 *
 * @author palacet
 */
public class NodeTests {
    Node n1;
    Node n2;
    
    
    @Before
    public void setUp()
    {
       Position p1 = new Position(50,50);
       Position p2 = new Position(100,100);
       n1 = new Node(p1);
       n2 = new Node(p2);
    }

    @After
    public void tearDown()
    {
        n1 = null;
        n2 = null;
    }
    
    @Test
    public void TypeTests()
    {
        n1.addType(new StartType());
        assertEquals("has start type once start type is set", n1.hasStartType(),true);
        assertEquals("doesnt have finish type if finish type isnt set", n1.hasFinishType(),false);
        
        n1.addType(new FinishType());
        assertEquals("has finish type when start type is set and then finish ",n1.hasFinishType(),true);
        assertEquals("has finish type when start type is set and then finish ",n1.hasStartType(),true);
        
        assertEquals("doesnt have start type upon construction", n2.hasStartType(),false);
        assertEquals("doesnt have finish type upon construction",n2.hasFinishType(),false);
        
        n1.addType(new FinishType());
        assertEquals("finish type propperly toggles",n1.hasFinishType(),false);
        n1.addType(new StartType());
        assertEquals("start type propperly toggles", n1.hasStartType(),false);
        
        n2.addType(new FinishType());
        n2.addType(new StartType());
        assertEquals("setting a NodeType to normal type clears it of finish type",n2.hasFinishType(),false);
        assertEquals("setting a NodeType to normal type clearss it of start type",n2.hasStartType(),false);
    }  
    
    @Test
    public void OtherNodeTests()
    {
        Position p3 = new Position(300,300);
        Position p4 = new Position(50,50);
        assertEquals("IsonPositon returns false when not on position",n1.isOnPosition(p3),false);
        assertEquals("IsonPositon returns true when on positon",n1.isOnPosition(p4),true);
        
        n1.setPosition(p4);
        assertEquals("setting a positon actually changes the position",p4,n1.getPosition());
        assertEquals("changing a position actually changes the positon",n1.isOnPosition(p4),true);
    }
    
}
