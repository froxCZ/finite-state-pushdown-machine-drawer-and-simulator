/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.union.fsm.gpu.tests;

import edu.union.fsm.gpu.models.Label;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;

import org.junit.Test;
import org.junit.Before;
import org.junit.After;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import javax.management.RuntimeErrorException;
import org.junit.Rule;
import org.junit.rules.ExpectedException;

/**
 *
 * @author Cheesecake
 */
@RunWith(JUnit4.class)
public class LabelTests {
    Label l1;
    Label l2;
    Label l3;
    
    @Before
    public void setUp()
    {
        l1 = new Label();
        l2 = new Label();
        l3 = new Label();
    }

    @After
    public void tearDown()
    {
        l1 = null;
        l2 = null;
        l3 = null;
    }
    
    @Test
    public void setLabelTest(){
	String l1String = "l1";
        String l2String = "";
        String newLabel = "new label";
        l1.setLabel(l1String);
        l2.setLabel(l2String);
        
        assertEquals("setLabel is propperly setting", l1.getLabel(),l1String);
        assertEquals("empty Label is propperly setting", l2.getLabel(),"");
        
        l1.setLabel(newLabel);
        
        assertEquals("Label chances when set label is called again", l1.getLabel(),newLabel);
        String l3String = null;
        l2.setLabel(l3String);
        assertEquals("changing a label to null", l2.getLabel(),null);
    }
    
    @Test
    public void hasSubscriptTests(){
        //hasExponetTests would be a redundant tests since code is the same...
        //just checking for a different symbol
        String subEnd = "Apple_";
        String subBegin = "_Orange";
        String subMiddle = "S_thirty";
        String subMult = "S_4_5_6";
        String noSub = "Regular";
        
        l1.setRawLabel(subEnd);
        l2.setRawLabel(subBegin);
        l3.setRawLabel(subMiddle);
        assertTrue("label has subscript at end", l1.hasSubscriptRaw());
        assertTrue("label has subscript at beginning", l2.hasSubscriptRaw());
        assertTrue("label has subscript in middle", l3.hasSubscriptRaw());
        
        l1.setRawLabel(subMult);
        assertTrue("hasSubscript() returns true when multiple subscript appears", l1.hasSubscriptRaw());
        
        l2.setRawLabel(noSub);
        assertFalse("Returns false when no subscript is in the label", l2.hasSubscriptRaw());
        
      
        assertFalse("using hasSubscript() method with no subscript in label or subscript",l2.hasSubscript());
        
        assertTrue("using hasSubscript() method with subscript in label returns true", l3.hasSubscript());
        
        l2.setSubscript("subbb");
        assertTrue("Label has a subscript, just not in the label", l2.hasSubscript());
          
    }
    
       
    
    @Test
    public void updateSubscriptTests(){
        String noSymb = "Regular";
        String beginSub = "_ABC";
        String endSub = "DeF_";
        String midSub = "R2_D2";
        
        l1.setRawLabel(noSymb);
        assertEquals("Should assign whole label to label if no _ is found", l1.getLabel(),noSymb);
        
        l2.setRawLabel(beginSub);
        assertEquals("subscript symbol at the beginning, no label", l2.getLabel(),"");
        assertEquals("subscript symbol at the beginning,rest is transfered to subscript",l2.getSubscript(),"ABC");
        
        l2.setRawLabel(noSymb);
        assertEquals("changing the rawLabel to one with no subscript removes the subscript",l2.getSubscript(),"");
        
        l3.setRawLabel(endSub);
        assertEquals("subscript symbol at the end, no subscript",l3.getSubscript(),"");
        assertEquals("subscript symbol at the end, whole thing is label",l3.getLabel(),"DeF");
        
        l3.setRawLabel(midSub);
        assertEquals("subscript symbol in the middle correctly seperates the label",l3.getLabel(), "R2");
        assertEquals("subscript symbol in the middle correctly seperates the subscript",l3.getSubscript(),"D2");
        
    }
    
    @Test
    public void updateExponentTests(){
        String noSymb = "Regular";
        String beginSub = "^ABC";
        String endSub = "DeF^";
        String midSub = "R2^D2";
        
        l1.setRawLabel(noSymb);
        assertEquals("Should assign whole label to label if no ^ is found", l1.getLabel(),noSymb);
        
        l2.setRawLabel(beginSub);
        
        assertEquals("Exponent symbol at the beginning, no label", l2.getLabel(),"");
        assertEquals("Exponent symbol at the beginning,rest is transfered to subscript",l2.getExponent(),"ABC");
        
        l2.setRawLabel(noSymb);
        assertEquals("changing the rawLabel to one with no exponent removes the subscript",l2.getExponent(),"");
        
        l3.setRawLabel(endSub);
        assertEquals("Exponent symbol at the end, no subscript",l3.getExponent(),"");
        assertEquals("exponent symbol at the end, whole thing is label",l3.getLabel(),"DeF");
        
        l3.setRawLabel(midSub);
        assertEquals("exponent symbol in the middle correctly seperates the label",l3.getLabel(), "R2");
        assertEquals("exponent symbol in the middle correctly seperates the subscript",l3.getExponent(),"D2");
        
    }
    
    @Test
    public void ExponentandSubscriptTests(){
        String expFirst = "A^B_C";
        String subFirst = "A_B^C";
        
        l1.setRawLabel(expFirst);
        assertEquals("correctly finds label, exponent first", l1.getLabel(),"A");
        assertEquals("correctly finds exponent, exponent first", l1.getExponent(),"B");
        assertEquals("correctly finds subscript, exponent first", l1.getSubscript(),"C");
        
        l2.setRawLabel(subFirst);
        assertEquals("correctly finds label, subscript first",l2.getLabel(),"A");
        assertEquals("correctly finds subscript, subscript first", l2.getSubscript(),"B");
        assertEquals("correctly finds exponent, subscript first",l2.getExponent(),"C");
    }
    
}
