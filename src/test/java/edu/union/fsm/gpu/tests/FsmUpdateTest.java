/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.union.fsm.gpu.tests;

import edu.union.fsm.gpu.FSMachine;
import edu.union.fsm.gpu.Machine;
import edu.union.fsm.gpu.FsmControl;
import edu.union.fsm.gpu.FsmListener;
import edu.union.fsm.gpu.helpers.Position;
import edu.union.fsm.gpu.models.Node;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import static org.junit.Assert.*;

/**
 *
 * @author frox
 */
@RunWith(JUnit4.class)
public class FsmUpdateTest {
    Machine fsm;
    MockController controller;
    @Before
    public void setUp()
    {
        fsm = new FSMachine();
        controller = new MockController(fsm);
    }    
    @Test
    public void basicTestUpdate(){
        fsm.addNode(new Node(new Position(1,2)));
        assertEquals("Wrong number of updates",1, controller.updateCnt);
        fsm.addNode(new Node(new Position(1,2)));
        assertEquals("Wrong number of updates",2, controller.updateCnt);
        fsm.addNode(new Node(new Position(1,2)));
        assertEquals("Wrong number of updates",3, controller.updateCnt);
        fsm.addNode(new Node(new Position(1,2)));
        assertEquals("Wrong number of updates",4, controller.updateCnt);
        fsm.remove(new Node(new Position(1,2)));
        assertEquals("Wrong number of updates",5, controller.updateCnt);        
    }
    private class MockController implements FsmListener{
        public int updateCnt;
        MockController(Machine f){
            f.setControlListener(this);
            updateCnt = 0;
        }
        @Override
        public void update() {
            updateCnt++;
        }
        
    }
}
